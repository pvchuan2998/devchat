import React from "react";
import ReactDOM from "react-dom";
import App from "./components/App";
import Login from "./components/Auth/Login";
import Register from "./components/Auth/Register";
import Spinner from "./Spinner";
import registerServiceWorker from "./registerServiceWorker";
import firebase from "./firebase";

import "semantic-ui-css/semantic.min.css";
import 'react-notifications/lib/notifications.css';


import {
  BrowserRouter as Router,
  Switch,
  Route,
  withRouter,
  Redirect
} from "react-router-dom";

import { createStore } from "redux";
import { Provider, connect } from "react-redux";
import { composeWithDevTools } from "redux-devtools-extension";
import rootReducer from "./reducers";
import { setUser, clearUser } from "./actions";
import ChartPage from "./components/Chart/ChartPage";
import Dashboard from "./components/Chart/Dashboard";
import ForgetPassword from "./components/Auth/ForgetPassword";
import MapComponent from "./components/Chart/MapComponent";
import DiseaseDetails from "./components/Chart/DiseaseDetails";
import NotFoundPage from "./components/Chart/NotFoundPage";
import DiseaseLibrary from "./components/Chart/DiseaseLibrary";
import HomepageLayout from "./components/HomepageLayout";
import MonthlyEnvironmentChart from "./components/Chart/MonthlyEnvironmentChart";
import UserProfile from "./components/Chart/UserProfile";


const store = createStore(rootReducer, composeWithDevTools());

class Root extends React.Component {

  state = {
    isAuthenticated: null
  }

  componentDidMount() {
    firebase.auth().onAuthStateChanged(user => {
      if (user) {
        // console.log(user);
        this.props.setUser(user);
        this.setState({isAuthenticated: true});
        //this.props.history.push("/");
      } else {
        //this.props.history.push("/login");
        this.props.history.push("/");
        this.setState({isAuthenticated: false});
        this.props.clearUser();
      }
    });
  }

  render() {
    const {isAuthenticated} = this.state;
    return this.props.isLoading ? (
      <Spinner />
    ) : (
      <Switch>
        <Route exact path="/" component={HomepageLayout} />
        <Route exact path="/chat" component={App} />
        {/* <Route path="/login" component={Login} /> */}
        <Route exact path="/login" render={() => ( !isAuthenticated ? <Login /> : <Redirect to='/' /> )} />
        <Route exact path="/register" component={Register} />
        <Route exact path="/export-data" component={ChartPage} />
        <Route exact path="/profile" component={UserProfile} />
        <Route exact path="/monthly-chart" component={MonthlyEnvironmentChart} />
        <Route exact path="/dashboard" component={Dashboard} /> 
        <Route exact path="/forget-password" component={ForgetPassword} /> 
        <Route exact path="/map" component={MapComponent} /> 
        <Route exact path="/library/:label" component={DiseaseDetails} /> 
        <Route exact path="/library" component={DiseaseLibrary} />
        <Route path="/404" component={NotFoundPage} />
        <Redirect to="/404" />
      </Switch>
    );
  }
}

const mapStateFromProps = state => ({
  isLoading: state.user.isLoading
});

const RootWithAuth = withRouter(
  connect(
    mapStateFromProps,
    { setUser, clearUser }
  )(Root)
);

ReactDOM.render(
  <Provider store={store}>
    <Router>
      <RootWithAuth />
    </Router>
  </Provider>,
  document.getElementById("root")
);
registerServiceWorker();
