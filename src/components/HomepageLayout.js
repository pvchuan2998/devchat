import { createMedia } from '@artsy/fresnel'
import PropTypes from 'prop-types'
import React, { Component } from 'react'
import firebase from "../firebase";
import {
  Button,
  Container,
  Grid,
  Header,
  Icon,
  Image,
  List,
  Menu,
  Segment,
  Sidebar,
  Visibility,
} from 'semantic-ui-react'
import {Link} from "react-router-dom";

const { MediaContextProvider, Media } = createMedia({
  breakpoints: {
    mobile: 0,
    tablet: 768,
    computer: 1024,
  },
})


const HomepageHeading = ({ mobile }) => (
  <Container text>
    <Header
      as='h2'
      content='Ứng dụng hỗ trợ thu thập thông tin nông nghiệp'
      inverted
      style={{
        fontSize: mobile ? '2em' : '3.5em',
        fontWeight: 'normal',
        marginBottom: 0,
        marginTop: mobile ? '1.5em' : '0.5em',
      }}
      
    />
    <Header
      as='h2'
      content='AgriCollect'
      inverted
      style={{
        fontSize: mobile ? '1.5em' : '6em',
        fontWeight: 'normal',
        marginTop: mobile ? '0.5em' : '0.1em',
      }}
    />
    <Image style={{ margin: "0 auto"}} src ='https://image.flaticon.com/icons/svg/1555/1555549.svg' size='medium'/>
    
  </Container>
)

HomepageHeading.propTypes = {
  mobile: PropTypes.bool,
}

class DesktopContainer extends Component {
  state = {isAuthenticated: null}

  hideFixedMenu = () => this.setState({ fixed: false })
  showFixedMenu = () => this.setState({ fixed: true })

  componentDidMount() {
    firebase.auth().onAuthStateChanged(user => {
      if (user) {
        this.setState({isAuthenticated: true});
      } else {
        this.setState({isAuthenticated: false});

      }
    });
  }

  handleSignout = () => {
    firebase
      .auth()
      .signOut()
      .then(() => console.log("signed out!"));
  };

  render() {
    const { children } = this.props
    const { fixed } = this.state

    return (
      <Media greaterThan='mobile'>
        <Visibility
          once={false}
          onBottomPassed={this.showFixedMenu}
          onBottomPassedReverse={this.hideFixedMenu}
        >
          <Segment
            inverted
            textAlign='center'
            style={{ minHeight: 700, padding: '1em 0em' }}
            vertical
          >
            <Menu
              fixed={fixed ? 'top' : null}
              inverted={!fixed}
              pointing={!fixed}
              secondary={!fixed}
              size='large'
            >
              <Container>
                <Menu.Item as={Link} to="/" active>
                  Trang chủ
                </Menu.Item>
               
                {!this.state.isAuthenticated ? 
                (
                  <Menu.Item position='right'>
                  <Button as={Link} to="/login" inverted={!fixed} >
                    Đăng nhập
                  </Button>
                  <Button as={Link} to="/register" inverted={!fixed} primary={fixed} style={{ marginLeft: '0.5em' }}>
                    Đăng ký
                  </Button>
                </Menu.Item>
                ) :
                  <Menu.Item position='right'>
                  {/* <Button as={Link} to="/library" primary={fixed} inverted={!fixed}>
                    Thư viện 
                  </Button>
                  <Button as={Link} to="/chat" inverted={!fixed} primary={fixed} style={{ marginLeft: '0.5em' }}>
                    Chat
                  </Button>
                  <Button as={Link} to="/monthly-chart" inverted={!fixed} primary={fixed} style={{ marginLeft: '0.5em' }}>
                    Biểu đồ
                  </Button>
                  <Button as={Link} to="/chart" inverted={!fixed} primary={fixed} style={{ marginLeft: '0.5em' }}>
                    Xem/xuất dữ liệu
                  </Button>
                  <Button as={Link} to="/map" inverted={!fixed} primary={fixed} style={{ marginLeft: '0.5em' }}>
                    Bản đồ sâu bệnh
                  </Button> */}
                  <Button as={Link} to="/dashboard" inverted={!fixed} primary={fixed} style={{ marginLeft: '0.5em' }}>
                    Bảng điều khiển
                  </Button>
                  <Button onClick={this.handleSignout} inverted={!fixed} primary={fixed} style={{ marginLeft: '0.5em' }}>
                    Đăng xuất
                  </Button>
                 
                </Menu.Item>
                }
                
              </Container>
            </Menu>
            <HomepageHeading />
          </Segment>
        </Visibility>

        {children}
      </Media>
    )
  }
}

DesktopContainer.propTypes = {
  children: PropTypes.node,
}

class MobileContainer extends Component {
  state = {}

  handleSidebarHide = () => this.setState({ sidebarOpened: false })

  handleToggle = () => this.setState({ sidebarOpened: true })

  render() {
    const { children } = this.props
    const { sidebarOpened } = this.state

    return (
      <Media as={Sidebar.Pushable} at='mobile'>
        <Sidebar.Pushable>
          <Sidebar
            as={Menu}
            animation='overlay'
            inverted
            onHide={this.handleSidebarHide}
            vertical
            visible={sidebarOpened}
          >
            <Menu.Item as={Link} to="/" active>
              Trang chủ
            </Menu.Item>
            <Menu.Item as={Link} to="/library">Thư viện</Menu.Item>
           
            <Menu.Item as={Link} to="/login">Đăng nhập</Menu.Item>
            <Menu.Item as={Link} to="/register">Đăng ký</Menu.Item>
          </Sidebar>

          <Sidebar.Pusher dimmed={sidebarOpened}>
            <Segment
              inverted
              textAlign='center'
              style={{ minHeight: 350, padding: '1em 0em' }}
              vertical
            >
              <Container>
                <Menu inverted pointing secondary size='large'>
                  <Menu.Item onClick={this.handleToggle}>
                    <Icon name='sidebar' />
                  </Menu.Item>
                  <Menu.Item position='right'>
                    <Button as={Link} to="/login" inverted>
                      Đăng nhập
                    </Button>
                    <Button as={Link} to="/register" inverted style={{ marginLeft: '0.5em' }}>
                      Đăng ký
                    </Button>
                  </Menu.Item>
                </Menu>
              </Container>
              <HomepageHeading mobile />
            </Segment>

            {children}
          </Sidebar.Pusher>
        </Sidebar.Pushable>
      </Media>
    )
  }
}

MobileContainer.propTypes = {
  children: PropTypes.node,
}

const ResponsiveContainer = ({ children }) => (
 
  <MediaContextProvider>
    <DesktopContainer>{children}</DesktopContainer>
    <MobileContainer>{children}</MobileContainer>
  </MediaContextProvider>
)

ResponsiveContainer.propTypes = {
  children: PropTypes.node,
}

const HomepageLayout = () => (
  <ResponsiveContainer>
    <Segment style={{ padding: '8em 0em' }} vertical>
      <Grid container stackable verticalAlign='middle'>
        <Grid.Row>
          <Grid.Column width={8}>
            <Header as='h3' style={{ fontSize: '3em', fontFamily:"Arial" }}>
              Thu thập thông tin môi trường
            </Header>
            <p style={{ fontSize: '1.33em' , fontFamily:"Arial"}}>
              Giúp thu thập các thông số về môi trường như nhiệt độ không khí, nhiệt độ đất,
              độ ẩm không khí, độ ẩm đất, độ pH,...
            </p>
            <Header as='h3' style={{ fontSize: '3em' , fontFamily:"Arial"}}>
              Nhận dạng và đo kích thước trái
            </Header>
            <p style={{ fontSize: '1.33em' , fontFamily:"Arial"}}>
             Nhận dạng và đo kích thước trái thông qua các thuật toán AI...
            </p>
            
          </Grid.Column>
          <Grid.Column floated='right' width={6}>
           
            <Header as='h3' style={{ fontSize: '3em', fontFamily:"Arial" }}>
              Nhận dạng sâu bệnh trên lá cây trồng
            </Header>
            <p style={{ fontSize: '1.33em', fontFamily:"Arial" }}>
             Nhận dạng các loại sâu bệnh qua ảnh chụp lá cây trồng...
            </p>
          </Grid.Column>
        </Grid.Row>
       
      </Grid>
    </Segment>

    <Segment color="blue" inverted vertical style={{ padding: '3em 0em', fontFamily: "Arial" }}>
            <Container>
              <Grid divided inverted stackable>
                <Grid.Row>
                  <Grid.Column width={3}>
                    <Header inverted as='h4' content='Các tính năng chính' />
                    <List link inverted>
                      <List.Item as='a'>Thu thập thông tin môi trường và trái</List.Item>
                      <List.Item as='a'>Biểu đồ thống kê</List.Item>
                      <List.Item as='a'>Nhận dạng sâu bệnh</List.Item>
                    </List>
                  </Grid.Column>
                  <Grid.Column width={3}>
                    <Header inverted as='h4' content='Đội ngũ phát triển' />
                    <List link inverted>
                      <List.Item as='a'>Phạm Văn Chuẩn</List.Item>
                      <List.Item as='a'>Nguyễn Ảnh Đạt</List.Item>
                      <List.Item as='a'>Phan Trần Thế Duy</List.Item>
                      <List.Item as='a'>Trần Quang Hưng</List.Item>
                    </List>
                  </Grid.Column>
                  <Grid.Column width={7} style={{marginLeft: "80px"}}>
                    <Header as='h4' inverted>
                    <Image size='small'  style={{marginRight: "10px"}} src ='https://image.flaticon.com/icons/svg/1555/1555549.svg'/> 
                        AgriCollect
                    <Image style={{marginLeft: "10px"}} src ='https://nhanlucnganhluat.vn/uploads/images/9426F66C/logo/2019-03/35228463_2076230712644398_4635641207110762496_n.png'/> 
                  
                    </Header>
                    <p>
                      Ứng dụng hỗ trợ thu thập thông tin nông nghiệp. Được phát triển bởi các sinh viên đến từ Trường Đại học Khoa học Tự nhiên TP. Hồ Chí Minh
                    </p>
                    <p>
                      227 Nguyễn Văn Cừ, P. 4, Q. 5, TP. Hồ Chí Minh
                    </p>
                  </Grid.Column>
                </Grid.Row>
              </Grid>
            </Container>
          </Segment>
  </ResponsiveContainer>
)

export default HomepageLayout