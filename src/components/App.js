import React from "react";
import { Grid, Button, Icon } from "semantic-ui-react";
import "./App.css"; 
import { connect } from "react-redux";
import ColorPanel from "./ColorPanel/ColorPanel";
import SidePanel from "./SidePanel/SidePanel";
import Messages from "./Messages/Messages";
import MetaPanel from "./MetaPanel/MetaPanel";
import { Link} from "react-router-dom";


const App = ({ currentUser, currentChannel, isPrivateChannel, userPosts, primaryColor, secondaryColor }) => (
  <Grid columns="equal" className="app" style={{ background: secondaryColor}}>
    <ColorPanel 
      key={currentUser && currentUser.name} 
      currentUser={currentUser}
      
    />
    <SidePanel key={currentUser && currentUser.uid} currentUser={currentUser}
      primaryColor={primaryColor}
    />

    <Grid.Column style={{ marginLeft: 320 }}>
      <Messages
        key={currentChannel && currentChannel.id}
        currentChannel={currentChannel}
        currentUser={currentUser}
        isPrivateChannel={isPrivateChannel}
      />
    </Grid.Column>

    <Grid.Column width={4}>
      <MetaPanel key={currentChannel && currentChannel.name} 
      isPrivateChannel={isPrivateChannel} currentChannel={currentChannel}
      userPosts={userPosts}/>

    {/* <Button style={{width:250}} size="big" inverted color='violet' icon labelPosition='right'>
      <Link style={{color: "black", fontFamily:"Arial", fontSize:"18px"}} to="/dashboard"> Bảng điều khiển </Link>
      
      <Icon name='right arrow' />
    </Button>
    <Button style={{width:250, marginTop:20}} size="big" inverted color='violet' icon labelPosition='right'>
      <Link style={{color: "black", fontFamily:"Arial", fontSize:"18px"}} to="/"> Trang chủ </Link>
      <Icon name='right arrow' />
    </Button> */}

    {/* <Button size="big" inverted color='violet' icon labelPosition='right'>
      <Link style={{color: "black", fontFamily:"Arial", fontSize:"18px"}} to="/map"> Dữ liệu thu thập và biểu đồ thống kê </Link>
      <Icon name='right arrow' />
    </Button> */}
    </Grid.Column>
  </Grid>
);

const mapStateToProps = state => ({
  currentUser: state.user.currentUser,
  currentChannel: state.channel.currentChannel,
  isPrivateChannel: state.channel.isPrivateChannel,
  userPosts: state.channel.userPosts,
  primaryColor: state.colors.primaryColor,
  secondaryColor: state.colors.secondaryColor
});

export default connect(mapStateToProps)(App);
