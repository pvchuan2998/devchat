import React, {Component} from "react";
import firebase from "../../firebase";
import {Grid, Form, Segment, Button, Header, Message, Image} from "semantic-ui-react";
import { Link, Redirect } from "react-router-dom";
import Footer from "../Chart/Footer";


class Login extends Component {
  state = {
    email: "",
    password: "",
    errors: [],
    loading: false,
    redirectToReferer: false
  };
  

  displayErrors = errors =>
    errors.map((error, i) => <p key={i}>{error.message}</p>);

  handleChange = event => {
    this.setState({ [event.target.name]: event.target.value });
  };

  handleSubmit = event => {
    event.preventDefault();
    if (this.isFormValid(this.state)) {
      this.setState({ errors: [], loading: true });
      firebase
        .auth()
        .signInWithEmailAndPassword(this.state.email, this.state.password)
        .then(signedInUser => {
          // setTimeout(() => this.props.history.push("/chat"), 3000);
          //setTimeout(() => <Redirect to="/dashboard"/>, 3000);
          //this.props.history.push("/");
          console.log(signedInUser);
        })
        .catch(err => {
          console.error(err);
          this.setState({
            errors: this.state.errors.concat(err),
            loading: false
          });
        });
    }
  };

  isFormValid = ({ email, password }) => email && password;

  handleInputError = (errors, inputName) => {
    return errors.some(error => error.message.toLowerCase().includes(inputName))
      ? "error"
      : "";
  };

  render() {
    const { email, password, errors, loading } = this.state;

    return (
      <React.Fragment >
      <Grid  textAlign="center" verticalAlign="middle" className="app">
        <Grid.Column style={{ maxWidth: 450 }}>
          <div style={{ width: "100%" }}>
            <Image style={{ margin: "0 auto" }} src ='https://image.flaticon.com/icons/svg/1555/1555549.svg' size='medium'/>
          </div>
          
          <Header as="h1" icon color="blue" textAlign="center">
             
            {/* <Icon name="code branch" color="violet" /> */}
            Đăng nhập vào AgriCollect
          </Header>
          <Form onSubmit={this.handleSubmit} size="large">
            <Segment stacked>
              <Form.Input
                fluid
                name="email"
                icon="mail"
                iconPosition="left"
                placeholder="Địa chỉ email"
                onChange={this.handleChange}
                value={email}
                className={this.handleInputError(errors, "email")}
                type="email"
              />

              <Form.Input
                fluid
                name="password"
                icon="lock"
                iconPosition="left"
                placeholder="Mật khẩu"
                onChange={this.handleChange}
                value={password}
                className={this.handleInputError(errors, "password")}
                type="password"
              />

              <Button
                disabled={loading}
                className={loading ? "loading" : ""}
                color="blue"
                fluid
                size="large"
              >
                Đăng nhập
              </Button>
            </Segment>
          </Form>
          {errors.length > 0 && (
            <Message error>
              <h3>Error</h3>
              {this.displayErrors(errors)}
            </Message>
          )}
          <Message>
            Chưa có tài khoản? <Link to="/register">Đăng ký</Link>
          </Message>
          <Message >
            Quên mật khẩu? <Link to="/forget-password">Nhấn vào đây</Link>
          </Message>
        </Grid.Column>
      </Grid>
      <div style={{marginTop: "15px"}}>
        <Footer />
      </div>
      
      </React.Fragment>
      
    );
  }
}

export default Login;

