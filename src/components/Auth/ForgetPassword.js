import React, { Component } from 'react';
import { Input, Button, Form, Grid, Image, Header } from 'semantic-ui-react';
import firebase from '../../firebase';

const INITIAL_STATE = {
    email: '',
    error: null,
  };

class ForgetPassword extends Component {
    constructor(props) {
        super(props);
    
        this.state = { ...INITIAL_STATE };
      }
    
      onSubmit = (event) => {
        const { email } = this.state;
    
        firebase.auth().sendPasswordResetEmail(email)
          .then(() => {
            this.setState(() => ({ ...INITIAL_STATE }));
          })
          .catch(error => {
            this.setState(() => ({ error }));
          });
    
        event.preventDefault();
      }
    
      render() {
        const {
          email,
          error,
        } = this.state;
    
        const isInvalid = email === '';
    
        return (
            <Grid textAlign="center" verticalAlign="middle" className="app">
                <Grid.Column style={{ maxWidth: 450 }}>
                    <div style={{ width: "100%" }}>
                        <Image style={{ margin: "0 auto" }} src ='https://image.flaticon.com/icons/svg/1555/1555549.svg' size='medium'/>
                    </div>
                    <Header as="h1" icon color="blue" textAlign="center">
                        {/* <Icon name="puzzle piece" color="orange" /> */}
                        Quên mật khẩu
                    </Header>

                    <Form className="centered"  onSubmit={this.onSubmit}>
                        <Input
                            placeholder="Email Address"
                            value={this.state.email}
                            onChange={event => this.setState({ email: event.target.value })}
                        />
                
                        <Button style={{marginLeft: "10px"}} disabled={isInvalid} type="submit">
                            Lấy lại mật khẩu
                        </Button>
                
                        { error && <p>{error.message}</p> }
                    </Form>
                
                </Grid.Column>
          </Grid>
        );

    
      }
}

export default ForgetPassword;