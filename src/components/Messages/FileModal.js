import React, {Component} from "react";
import mime from "mime-types";
import { Modal, Input, Button, Icon } from "semantic-ui-react";

class FileModal extends Component {
  state = { file: null, authorized: ["image/jpeg", "image/png"] };

  addFile = event => {
    const file = event.target.files[0];
    if (file) {
      this.setState({ file: file });
    }
  };

  sendFile = () => {
    const { file } = this.state;
    const { uploadFile, closeModal } = this.props; //from MessageForm

    if (file !== null) {
      if (this.isAuthorized(file.name)) {
        const metadata = { contentType: mime.lookup(file.name) };
        uploadFile(file, metadata);
        closeModal();
        this.clearFile();
      }
    }
  };

  //check image type
  isAuthorized = filename => this.state.authorized.includes(mime.lookup(filename));

  clearFile = () => this.setState({ file: null });

  render() {
    const { modal, closeModal } = this.props; //from MessageForm

    return (
      <Modal basic open={modal} onClose={closeModal}>
        <Modal.Header>Chọn một ảnh</Modal.Header>
        <Modal.Content>
          <Input onChange={this.addFile} fluid label="File types: jpg, png" name="file" type="file"/>
        </Modal.Content>
        <Modal.Actions>
          <Button onClick={this.sendFile} color="green" inverted>
            <Icon name="checkmark" /> Gửi
          </Button>
          <Button color="red" inverted onClick={closeModal}>
            <Icon name="remove" /> Hủy
          </Button>
        </Modal.Actions>
      </Modal>
    );
  }
}

export default FileModal;


