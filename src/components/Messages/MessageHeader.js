import React, { Component } from 'react';
import {Segment, Header, Input, Icon} from 'semantic-ui-react';

class MessageHeader extends Component {
    render() {
        const {channelName, numUniqueUsers, handleSearchChange, searchLoading} = this.props;
        return (
            <Segment clearing>
                {/* Channel Title */}
                <Header fluid="true" as="h2" floated="left" style={{marginBottom: 0}}>
                    <span >
                        {channelName}
                        <Icon  name={"star outline"} color="black" />
                    </span>
                    <Header.Subheader>{numUniqueUsers}</Header.Subheader>
                </Header>
                {/* Channel search input */}
                <Header floated="right">
                    <Input size="mini" icon="search" 
                    name="searchTerm" placeholder="Tìm tin nhắn"
                    onChange={handleSearchChange}
                    loading={searchLoading}/>
                </Header>
            </Segment>
        );
    }
}

export default MessageHeader;