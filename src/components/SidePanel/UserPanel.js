import React from "react";
import firebase from "../../firebase";
import { Grid, Header, Icon, Dropdown, Image, Modal, Input, Button } from "semantic-ui-react";
import AvatarEditor from 'react-avatar-editor';

class UserPanel extends React.Component {
  state = {
    user: this.props.currentUser,
    modal: false,
    previewImage: '',
    croppedImage: '',
    blob: '',
    storageRef: firebase.storage().ref(),
    userRef: firebase.auth().currentUser,
    usersRef: firebase.database().ref('users'),
    metadata: {
      contentType: 'image/jpeg'
    },
    uploadedCroppedImage: ''
  };

  openModal = () => this.setState({modal: true});
  closeModal = () => this.setState({modal:false});

  dropdownOptions = () => [
    {
      key: "user",
      text: (
        <span>
          Đăng nhập dưới tên <strong>{this.state.user.displayName}</strong>
        </span>
      ),
      disabled: true
    },
    {
      key: "avatar",
      text: <span onClick={this.openModal}>Thay ảnh đại diện</span>
    },
    {
      key: "signout",
      text: <span onClick={this.handleSignout}>Đăng xuất</span>
    }
  ];

  handleSignout = () => {
    firebase
      .auth()
      .signOut()
      .then(() => console.log("signed out!"));
  };

  handleChange = event => {
    const file = event.target.files[0]
    const reader = new FileReader();

    if(file) {
      reader.readAsDataURL(file);
      reader.addEventListener('load', () => {
        this.setState({previewImage: reader.result});
      })
    }
  }

  handleCropImage = () => {
    if(this.avatarEditor) {
      this.avatarEditor.getImageScaledToCanvas().toBlob(blob => {
        let imageUrl = URL.createObjectURL(blob);
        this.setState({
          croppedImage: imageUrl,
          blob
        });
      })
    }
  }

  uploadCroppedImage = () => {
    const {storageRef, userRef, metadata, blob} = this.state;

    storageRef
      .child(`avatars/user/${userRef.uid}`)
      .put(blob, metadata)
      .then(snap => {
        snap.ref.getDownloadURL().then(downloadURL => {
          this.setState({uploadedCroppedImage: downloadURL}, () => 
            this.changeAvatar())
        })
      })
  }

  changeAvatar = () => {
    this.state.userRef
      .updateProfile({
        photoURL:this.state.uploadedCroppedImage
      })
      .then(() => {
        console.log('photoUrl updated!')
        this.closeModal();
      })
      .catch(err => {
        console.error(err)
      })

      this.state.usersRef
        .child(this.state.user.uid)
        .update({avatar: this.state.uploadedCroppedImage})
        .then(() => {
          console.log('avatar updated')
        })
        .catch(err => {
          console.error(err)
        })
  }

  render() {
    const { user, modal, previewImage, croppedImage } = this.state;
    const {primaryColor} = this.props;

    return (
      <Grid style={{ background: primaryColor }}>
        <Grid.Column>
          <Grid.Row style={{ padding: "1.2em", margin: 0 }}>
            {/* App Header */}
            <Header inverted floated="left" as="h2">
              <Icon  name="rocketchat"/>
              <Header.Content>AgriCollect</Header.Content>
            </Header>

            {/* User Dropdown  */}
            <Header style={{ padding: "0.25em" }} as="h4" inverted>
              <Dropdown
                trigger={
                  <span>
                    <Image src={user.photoURL} spaced="right" avatar />
                    {user.displayName}
                  </span>
                }
                options={this.dropdownOptions()}
              />
            </Header>

            <Header style={{ padding: "0.25em" }} as="h4" inverted>
              {/* <Link to="/chart"> Biểu đồ dữ liệu </Link> */}
              {/* <Link to="/dashboard"> Dữ liệu thu thập và biểu đồ thống kê </Link> */}
            </Header>
          </Grid.Row>


          {/* Change User Avatar */}
          <Modal basic open={modal} onClose={this.closeModal}>
            <Modal.Header>Thay ảnh đại diện</Modal.Header>
            <Modal.Content>
              <Input onChange={this.handleChange} fluid type="file" label="Ảnh đại diện mới" name="previewImage" />
              <Grid centered stackable columns={2}>
                <Grid.Row centered>
                  <Grid.Column className="ui center aligned grid">
                    {/* Image Preview */}
                    {previewImage && (
                      <AvatarEditor 
                        image={previewImage} width={120} 
                        height={120} border={50} scale={1.2} 
                        ref={node => (this.avatarEditor = node)}
                      />
                    )}
                  </Grid.Column>
                  <Grid.Column>
                    {/* Cropped Image Preview */}
                    {croppedImage && (<Image style={{margin: '3.5em auto'}} width={100} height={100}
                      src={croppedImage}/>)}
                  </Grid.Column>
                </Grid.Row>
              </Grid>
            </Modal.Content>
            <Modal.Actions>
              {croppedImage && <Button color="green" inverted onClick={this.uploadCroppedImage}>
                <Icon name="save" /> Thay ảnh đại diện
              </Button>}
              <Button color="green" inverted onClick={this.handleCropImage}>
                <Icon name="image" /> Xem trước
              </Button>
              <Button color="red" inverted onClick={this.closeModal}>
                <Icon name="remove" /> Hủy
              </Button>
            </Modal.Actions>
          </Modal>
        </Grid.Column>
      </Grid>
    );
  }
}

export default UserPanel;
