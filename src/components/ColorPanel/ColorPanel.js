import React, {Component} from "react";
import { Sidebar, Menu, Divider, Button,  Modal, Icon, Label, Segment} from "semantic-ui-react";
import {SliderPicker} from 'react-color';
import firebase from "../../firebase";
import {connect} from 'react-redux';
import {setColors} from '../../actions';

class ColorPanel extends Component {

  state = {
    modal: false,
    primary: "",
    secondary: "",
    usersRef: firebase.database().ref('users'),
    user: this.props.currentUser,
    userColors: []
  }

  componentDidMount() {
    if(this.state.user){
      this.addListener(this.state.user.uid);
    }
  }

  componentWillUnmount() {
    this.removeListener();
  }

  removeListener = () => {
    this.state.usersRef.child(`${this.state.user.uid}/colors`).off();
  }
  
  //realtime listener colors added
  addListener = userId => {
    let userColors = [];
    this.state.usersRef
      .child(`${userId}/colors`)
      .on('child_added', snap => {
        //add one or more element into arr and return the new arr
        userColors.unshift(snap.val());
        this.setState({userColors});
      })

  }
  

  openModal = () => this.setState({modal: true});

  closeModal = () => this.setState({modal: false});

  handleChangePrimary = color => this.setState({primary: color.hex});

  handleChangeSecondary = color => this.setState({secondary: color.hex});

  handleSaveColors = () => {
    if(this.state.primary && this.state.secondary){
       this.saveColors(this.state.primary, this.state.secondary) 
    }
  }
  

  saveColors = (primary, secondary) => {
    this.state.usersRef
      .child(`${this.state.user.uid}/colors`)
      .push()
      .update({
        primary,
        secondary
      })
      .then(() => {
        console.log("Save added!")
        this.closeModal();
      })
      .catch(err => console.error(err))
  }

  displayUserColors = colors => (
    colors.length > 0 && colors.map((color, i) => (
      <React.Fragment key={i}>
        <Divider />
          <div title="Đổi sang màu này" className="color__container" onClick={() => this.props.setColors(color.primary, color.secondary)}>
            <div className="color__square" style={{background: color.primary}}>
              <div className="color__overlay" style={{background: color.secondary}}>

              </div>
            </div>
          </div>
      </React.Fragment>
    ))
  )


  render() {
    const {modal, primary, secondary, userColors} = this.state;
    return (
      <Sidebar as={Menu} visible width="very thin" icon="labeled" inverted vertical>
        <Divider />
        <Button title="Nhấn vào đây để thêm màu" icon="add" size="small" color="blue" onClick={this.openModal}/>
        {this.displayUserColors(userColors)}
        {/* Color picker modal */}
        <Modal basic open={modal} onClose={this.closeModal}>
          <Modal.Header>Thay đổi màu nền ứng dụng</Modal.Header>

          <Modal.Content>
            <Segment inverted>
              <Label content="Primary Color" />
              <SliderPicker color={primary} onChange={this.handleChangePrimary}/> 
            </Segment>

            <Segment inverted>
              <Label content="Secondary Color" />
              <SliderPicker color={secondary} onChange={this.handleChangeSecondary}/>
            </Segment>
          </Modal.Content>

          <Modal.Actions>

            <Button color="green" inverted onClick={this.handleSaveColors}>
                <Icon name="checkmark"/> Lưu màu
            </Button>

            <Button color="red" inverted onClick={this.closeModal}>
                <Icon name="remove"/> Hủy
            </Button>

          </Modal.Actions>

        </Modal>
      </Sidebar>
    );
  }
}

export default connect(null, {setColors})(ColorPanel);
