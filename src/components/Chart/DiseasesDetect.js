import React, { Component } from 'react';
import {Card} from 'semantic-ui-react';
import firebase from '../../firebase';
import DiseaseCard from './DiseaseCard';




class DiseasesDetect extends Component {

    _isMounted = false;

    
    constructor(props) {
        super(props);
        
        this.state = {
            currentOrchardName: "VuonSongLu",
            orchardName: [],
            diseasesData: []
        }

        this.handleChangeName = this.handleChangeName.bind(this);
    }
    

    fetchOrchard = () => {
        const db = firebase.firestore()

        db.collection("Orchard")
        .get()
        .then(querySnapshot => {
            const id = querySnapshot.docs.map(doc => doc.id);

            if(this._isMounted){
                this.setState({ orchardName: id});
                
            }
        });
    }

    fetchDiseasesData = (id) => {
        const db = firebase.firestore()

        db.collection("DiseasesDetectorResults")
        .where('orchardId', '==', id)
        .orderBy('time', 'desc')
        .get()
        .then(querySnapshot => {
            const data = querySnapshot.docs.map(doc => doc.data());

            if(this._isMounted){
                this.setState({ diseasesData: data});
                console.log(data)
            }
        });
    }

    handleChangeName(event) {
        this.setState({currentOrchardName: event.target.value});
        console.log(this.state.currentOrchardName)
    }

    componentDidMount() {
        this._isMounted = true;
        this.fetchOrchard()
        this.fetchDiseasesData(this.state.currentOrchardName)
    }

    componentDidUpdate(prevProps, prevState) {
        if(prevState.currentOrchardName !== this.state.currentOrchardName){
            this.fetchDiseasesData(this.state.currentOrchardName)
        }
    }
    

    componentWillUnmount() {
        this._isMounted = false;
    }
    
    render() {
        const {orchardName, diseasesData} = this.state
        return (
            <div style={{width: "100%", background:"#eee"}}>
                <Card style={{width: "40%", margin: "5px auto"}}>
                    <Card.Content style={{ margin: "0px auto"}}>
                        <form onSubmit={this.handleSubmit}>
                            <label style={{fontSize: "22px", marginRight:"5px"}}>
                            Chọn khu vườn:
                            </label>
                            <select style={{fontSize: "22px"}} onChange={this.handleChangeName} value={this.state.currentOrchardName}>
                                {orchardName.map(( id, index) => 
                                    <option value={id} key={index} >
                                        {id}
                                    </option>
                                )}
                            </select>
                        </form>
                    </Card.Content>
                </Card>

                <DiseaseCard data={diseasesData}/>
            </div>
        );
    }
}

export default DiseasesDetect;