import React, { Component } from 'react';
import {Card, Breadcrumb} from 'semantic-ui-react';
import firebase from '../../firebase';
import OwnerCard from './OwnerCard';
import OrchardInfo from './OrchardInfo';
import MonthlyLineChart from './MonthlyLineChart';
import { Link } from "react-router-dom";

const monthOptions = ['1', '2', '3', '4', '5', '6', '7', '8', '9', '10', '11', '12']

class MonthlyEnvironmentChart extends Component {

    _isMounted = false;

    constructor(props) {
        super(props);
        
        this.state = {
            orchardName: [],
            currentOrchardName: 'VuonSongLu',
            orchardData :[],
            enviData: [],
            currentOwnerId: '',
            currentOrchardData: {},
            currentMonth: '1', 
            orchardId: []
        }

        this.handleChange = this.handleChange.bind(this);
        this.handleChangeName = this.handleChangeName.bind(this);
    }
    
   
    fetchMonthlyData = (orchardId, month) => {
        const db = firebase.firestore()
        // let start = new Date('2020-07-11')
        // let end = new Date('2020-07-14')
        let start 
        let end 
        if(month === "1"){
            start = new Date('2020-01-01')
            end = new Date('2020-01-31')
        }
        if(month === "2"){
            start = new Date('2020-02-01')
            end = new Date('2020-02-29')
        }
        if(month === "3"){
            start = new Date('2020-03-01')
            end = new Date('2020-03-31')
        }
        if(month === "4"){
            start = new Date('2020-04-01')
            end = new Date('2020-04-30')
        }
        if(month === "5"){
            start = new Date('2020-05-01')
            end = new Date('2020-05-31')
        }
        if(month === "6"){
            start = new Date('2020-06-01')
            end = new Date('2020-06-30')
        }
        if(month === "7"){
            start = new Date('2020-07-01')
            end = new Date('2020-07-31')
        }
        if(month === "8"){
            start = new Date('2020-08-01')
            end = new Date('2020-08-31')
        }
        if(month === "9"){
            start = new Date('2020-09-01')
            end = new Date('2020-09-30')
        }
        if(month === "10"){
            start = new Date('2020-10-01')
            end = new Date('2020-10-31')
        }
        if(month === "11"){
            start = new Date('2020-11-01')
            end = new Date('2020-11-30')
        }
        if(month === "12"){
            start = new Date('2020-12-01')
            end = new Date('2020-12-31')
        }

        db.collection("KetQuaDo")
            .where('maSoKhuVuon', '==', orchardId)
            .where('time', '>=', start)
            .where('time', '<=', end)
            .get()
            .then(querySnapshot => {
                const data = querySnapshot.docs.map(doc => doc.data());

                 if(this._isMounted){
                    this.setState({ enviData: data});
                    console.log(data)
                }
            });

    }

    fetchCurrentOrchard = (id) => {
        const db = firebase.firestore()

        db.collection("Orchard")
        .doc(id)
        .get()
        .then(doc => {
            const data = doc.data();
            
            if(this._isMounted){
                this.setState({ currentOrchardData: data});
                this.setState({currentOwnerId: data.owner});
            }
            
        });
    }

    fetchOrchard = () => {
        const db = firebase.firestore()

        db.collection("Orchard")
        .get()
        .then(querySnapshot => {
            const orchardName = querySnapshot.docs.map(doc => doc.data().name);
            const data = querySnapshot.docs.map(doc => doc.data());
            const id = querySnapshot.docs.map(doc => doc.id);
            if(this._isMounted){
                this.setState({ orchardName: orchardName, orchardData: data, orchardId: id});
                
            }
        });
    }

    componentDidMount() {
        this._isMounted = true;
        //this.fetchMonthlyData(this.state.currentOrchardName, this.state.currentMonth);
        this.fetchOrchard();
        this.fetchCurrentOrchard(this.state.currentOrchardName);
    }

    componentDidUpdate(prevProps, prevState) {
        if ((prevState.currentMonth!== this.state.currentMonth) 
            || (prevState.currentOrchardName !== this.state.currentOrchardName)) 
        {
            //this.fetchMonthlyData(this.state.currentOrchardName, this.state.currentMonth);
            this.fetchCurrentOrchard(this.state.currentOrchardName);
        }
    }

 
    handleChange(event) {
        this.setState({currentMonth: event.target.value});
    }
    handleChangeName(event) {
        this.setState({currentOrchardName: event.target.value});
        console.log(this.state.currentOrchardName)
    }

    componentWillUnmount() {
        this._isMounted = false;
    }
    
    render() {
        const { orchardId, currentOrchardData, currentOwnerId, currentOrchardName, currentMonth } = this.state
        return (

            <div style={{width: "100%"}}>
                 {/* <Breadcrumb size="massive" style={{fontFamily:"Arial", margin:20}}>
                    <Breadcrumb.Section link><Link to="/">Trang chủ</Link></Breadcrumb.Section>
                    <Breadcrumb.Divider icon='right chevron' />
                    <Breadcrumb.Section active>Biểu đồ dữ liệu</Breadcrumb.Section>
                </Breadcrumb> */}
                <Card style={{width: "40%", margin: "5px auto"}}>
                    <Card.Content style={{ margin: "0px auto"}}>
                        <form onSubmit={this.handleSubmit}>
                            <label style={{fontSize: "22px", marginRight:"5px"}}>
                            Chọn khu vườn:
                            </label>
                            <select style={{fontSize: "22px"}} onChange={this.handleChangeName} value={this.state.currentOrchardName}>
                                {orchardId.map(( id, index) => 
                                    <option value={id} key={index} >
                                        {id}
                                    </option>
                                )}
                            </select>
                        </form>
                    </Card.Content>
                </Card>
                <Card style={{width: "40%", margin: "5px auto"}}>
                    <Card.Content style={{ margin: "0px auto"}}>
                        <form onSubmit={this.handleSubmit}>
                            <label style={{fontSize: "22px", marginRight:"5px"}}>
                            Chọn tháng:
                            </label>
                            <select style={{fontSize: "22px"}} onChange={this.handleChange} value={this.state.currentMonth}>
                                {monthOptions.map(m => 
                                    <option value={m} key={m} >
                                        Tháng {m}
                                    </option>
                                )}
                            </select>
                        </form>
                    </Card.Content>
                </Card>

                <OrchardInfo currentOrchardData={currentOrchardData}/>

                <OwnerCard currentOwnerId={currentOwnerId}/>

                <MonthlyLineChart currentOrchardName={currentOrchardName} currentMonth={currentMonth} />
            </div>
             
        );
    }
}

export default MonthlyEnvironmentChart;