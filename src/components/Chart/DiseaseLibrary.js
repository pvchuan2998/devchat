import React, { Component } from 'react';
import {Card, Icon, Grid, Button, Breadcrumb} from 'semantic-ui-react';
import firebase from '../../firebase';
import { Redirect, Link } from "react-router-dom";
import SearchBox from './SearchBox'
import './searchbox.css'


class DiseaseLibrary extends Component {

    _isMounted = false;

    state = {
        data: [],
        searchField:''
    }

    fetchDiseaseData = () => {
        const db = firebase.firestore()

        db.collection("DiseaseDetails")
        .get()
        .then(querySnapshot => {
            const data = querySnapshot.docs.map(doc => doc.data());

            if(this._isMounted){
                this.setState({data});
                //console.log(data)
            }
        });
    }

    componentDidMount() {
        
        this._isMounted = true;
        this.fetchDiseaseData();
        
    }
    
    
    componentWillUnmount() {
        this._isMounted = false;
    }
    
    handleChange = (e) =>{
        this.setState({searchField:e.target.value})
    }
    

    render() {
        const {data, searchField} = this.state;
        
        const filteredDiseases = data.filter(data =>(
            data.name.toLowerCase().includes(searchField.toLowerCase())
          ))
        return (
            <React.Fragment>
                {/* <Breadcrumb size="massive" style={{fontFamily:"Arial", margin:20}}>
                <Breadcrumb.Section link><Link to="/">Trang chủ</Link></Breadcrumb.Section>
                    <Breadcrumb.Divider icon='right chevron' />
                    <Breadcrumb.Section active>Thư viện</Breadcrumb.Section>
                </Breadcrumb> */}
                
                    <div className="wrapdiv">
                        <div className="searchdiv">
                            <SearchBox  placeholder="Tìm kiếm..." handleChange={this.handleChange}/>
                        </div>
                    
                    </div>
    
                <Grid columns="equal" style={{ marginLeft: 200 , marginBottom: 20, marginTop: 20}}>
                
                {filteredDiseases && filteredDiseases.map((data, i) => 
                <div style={{marginBottom:10}}>
                    <Card key={i} onClick={() => <Redirect />}>
                    {/* <Image size="medium" src={data.imageUrl} wrapped ui={false} /> */}
                    <img src={data.imageUrl} alt="diseaseimage" width="290" height="300"/>
                    <Card.Content>
                        <Card.Header>{data.name}</Card.Header>
                        {/* <Card.Meta>{data.vnName}</Card.Meta> */}
                        <Card.Description style={{fontFamily:"Arial"}}>
                            {data.vnName}
                        </Card.Description>
                    </Card.Content>
                    <Card.Content extra style={{display:"flex", flexDirection:"column"}}>
                    <a style={{marginBottom:10}}>
                        <Icon name='bug' />
                        {data.type}
                    </a>
                   
                    <Button size="mini" inverted color='orange' icon labelPosition='right'>
                        <Link style={{color: "black", fontFamily:"Arial", fontSize:"14px"}} to={`/library/${data.label}`}> Xem thêm </Link>
                        <Icon name='right arrow' />
                    </Button>

                    </Card.Content>
                </Card>
                </div>
                
            )}
            </Grid>
            </React.Fragment>
            
            
        );
    }
}

export default (DiseaseLibrary);