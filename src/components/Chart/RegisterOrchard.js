import React, { Component } from 'react';
import {Button, Form, Input, TextArea, Message, Header, Icon} from 'semantic-ui-react';
import firebase from "../../firebase";
import { NotificationContainer, NotificationManager } from "react-notifications";



class RegisterOrchard extends Component {

    state = {
        formValues: {
            name: "",
            address: "",
            area: "",
            type: "",
            owner: "",
            id: "",
            
        },
        formOwnerValues: {
            name: "",
            gender: "",
            phone: "",
            birthday: "",
            address: "",
            identificationID: "",
            ownerId: ""
        },
        errors: []
    }

    
    handleChange = e => this.setState({
        formValues: {...this.state.formValues, [e.target.name]: e.target.value }
    });

    handleChangeSubmitOwner = e => this.setState({
        formOwnerValues: {...this.state.formOwnerValues, [e.target.name]: e.target.value }
    });
    

    handleSubmit = event => {
        event.preventDefault();
        if (this.isFormValid() && this.isFormOwnerValid()){
            this.setState({ errors: []});
            const db = firebase.firestore()
            
            const data ={
                ...this.state.formValues
            }

            const dataOwner ={
                ...this.state.formOwnerValues
            }

            data.owner = dataOwner.ownerId
           
            db.collection("Orchard")
            .doc(data.id)
            .set(data)
            .then(() => {
                NotificationManager.success("Một khu vườn đã được thêm vào hệ thống!", "Thành công!");
            })
            .catch(error => {
                NotificationManager.error(error.message, "Thêm vườn không thành công!");
                this.setState({
                    errors: this.state.errors.concat(error)
                    });
                });
        }
        
    }

    handleSubmitOwner = event => {
        event.preventDefault();
        if (this.isFormValid() && this.isFormOwnerValid()){
            this.setState({ errors: []});
            const db = firebase.firestore()
            
            const data ={
                ...this.state.formOwnerValues
            }
           
            db.collection("Owner")
            .doc(data.ownerId)
            .set(data)
            .then(() => {
                NotificationManager.success("Một chủ vườn đã được thêm vào hệ thống!", "Thành công!");
            })
            .catch(error => {
                NotificationManager.error(error.message, "Thêm một chủ vườn không thành công!");
                this.setState({
                    errors: this.state.errors.concat(error)
                    });
                });
        }
        
    }

    isFormEmpty = ({ name, address, area, type, id}) => {
        return (
          !name.length ||
          !address.length ||
          !area.length ||
          !type.length ||
          !id
        );
      };

      isFormOwnerEmpty = ({ name, address, gender, birthday, ownerId, identificationID, phone  }) => {
        return (
          !name.length ||
          !address.length ||
          !ownerId.length ||
          !gender.length ||
          !birthday.length ||
          !identificationID.length ||
          !phone.length
        );
      };
    
    isFormValid = () => {
        let errors = [];
        let error;

        if (this.isFormEmpty(this.state.formValues)) {
            error = { message: "Vui lòng điền vào tất cả các vùng!" };
            this.setState({ errors: errors.concat(error) });
            return false;
        }else {
            return true;
          }
    }

    isFormOwnerValid = () => {
        let errors = [];
        let error;

        if (this.isFormOwnerEmpty(this.state.formOwnerValues)) {
            error = { message: "Vui lòng điền vào tất cả các vùng!" };
            this.setState({ errors: errors.concat(error) });
            return false;
        }else {
            return true;
          }
    }

    displayErrors = errors =>
        errors.map((error, i) => <p key={i}>{error.message}</p>);

    render() {
        const { formValues, formOwnerValues, errors } = this.state
        return (
            <div className="ui inverted segment" style={{width: "100%", background: "#eee"}}>
                <div style={{width: "60%", margin: "0 auto"}}>
                <Header style={{marginBottom: "20px"}} as="h1" icon color="blue" textAlign="center">
                    <Icon name="registered outline" color="blue" />
                    Đăng ký khu vườn
                </Header>
                <Form onSubmit={this.handleSubmitOwner}>
                    <Form.Group  widths='equal'>
                        <Form.Field
                            //id='form-input-control-first-name'
                            control={Input}
                            label='Tên chủ vườn'
                            placeholder='Tên chủ vườn'
                            value={formOwnerValues.name}
                            onChange={this.handleChangeSubmitOwner}
                            name="name"
                        />
                        <Form.Field
                            //id='form-input-control-last-name'
                            control={Input}
                            label='Giới tính'
                            placeholder='Nam/Nữ'
                            value={formOwnerValues.gender}
                            onChange={this.handleChangeSubmitOwner}
                            name="gender"
                        />
                        <Form.Field
                            //id='form-input-control-last-name'
                            control={Input}
                            label='Số điện thoại'
                            placeholder='Số điện thoại'
                            value={formOwnerValues.phone}
                            onChange={this.handleChangeSubmitOwner}
                            name="phone"
                        />
                    </Form.Group>
                    <Form.Field
                        //id='form-input-control-error-type'
                        control={Input}
                        label='Địa chỉ chủ vườn'
                        placeholder='Địa chỉ'
                        value={formOwnerValues.address}
                        onChange={this.handleChangeSubmitOwner}
                        name="address"
                    />

                    <Form.Group  widths='equal'>
                        <Form.Field
                            //id='form-input-control-first-name'
                            control={Input}
                            label='CMND/CCND'
                            placeholder='CMND/CCND'
                            value={formOwnerValues.identificationID}
                            onChange={this.handleChangeSubmitOwner}
                            name="identificationID"
                        />
                        <Form.Field
                            //id='form-input-control-last-name'
                            control={Input}
                            label='Ngày sinh'
                            placeholder='Ngày sinh'
                            value={formOwnerValues.birthday}
                            onChange={this.handleChangeSubmitOwner}
                            name="birthday"
                        />
                        <Form.Field
                            //id='form-input-control-last-name'
                            control={Input}
                            label='Mã chủ vườn'
                            placeholder='Tên chủ vườn viết liền không dấu'
                            value={formOwnerValues.ownerId}
                            onChange={this.handleChangeSubmitOwner}
                            name="ownerId"
                        />
                    
                    </Form.Group>

                    <Form.Field
                        //id='form-button-control-public'
                        style={{marginBottom: "15px"}}
                        control={Button}
                        content='Xác nhận'
                        type="submit"
                        
                    />
                </Form>

                <Form onSubmit={this.handleSubmit}>
                    <Form.Group  widths='equal'>
                        <Form.Field
                            //id='form-input-control-first-name'
                            control={Input}
                            label='Tên vườn'
                            placeholder='Tên vườn'
                            value={formValues.name}
                            onChange={this.handleChange}
                            name="name"
                        />
                        <Form.Field
                            //id='form-input-control-last-name'
                            control={Input}
                            label='Diện tích'
                            placeholder='Diện tích'
                            value={formValues.area}
                            onChange={this.handleChange}
                            name="area"
                        />
                        <Form.Field
                            //id='form-input-control-last-name'
                            control={Input}
                            label='Mã vườn'
                            placeholder='Tên vườn viết liền không dấu'
                            value={formValues.id}
                            onChange={this.handleChange}
                            name="id"
                        />
                    </Form.Group>

                    <Form.Field
                        //id='form-textarea-control-opinion'
                        control={TextArea}
                        label='Địa chỉ vườn'
                        placeholder='Địa chỉ vườn'
                        value={formValues.address}
                        onChange={this.handleChange}
                        name="address"
                    />
                    <Form.Field
                        //id='form-input-control-error-type'
                        control={Input}
                        label='Loại đất canh tác'
                        placeholder='Loại đất canh tác'
                        value={formValues.type}
                        onChange={this.handleChange}
                        name="type"
                    />

                    
                    
                    <Form.Field
                        //id='form-button-control-public'
                        style={{marginBottom: "15px"}}
                        control={Button}
                        content='Xác nhận'
                        type="submit"
                        
                    />
                        
                </Form>
                    {errors.length > 0 && (
                        <Message error>
                        <h3>Lỗi!</h3>
                        {this.displayErrors(errors)}
                        </Message>
                    )}
                </div>
                <NotificationContainer/>
            </div>
           
        );
    }
}

export default RegisterOrchard;