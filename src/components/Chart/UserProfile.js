import React, { Component } from 'react';
import {Card, Image, Icon} from "semantic-ui-react";
import firebase from '../../firebase';

class UserProfile extends Component {
    state = {
        userRef: firebase.auth().currentUser,
    }

 
    render() {
        const {userRef} = this.state;
        return (
            <div style={{width: "100%", marginTop:"7%"}}>
                
                <Card style={{width: "40%", margin: "0px auto"}}> 
                    <Image centered size="medium" src={userRef.photoURL} />
                    <Card.Content textAlign="center">
                        <Card.Header>Tên người dùng: {userRef && userRef.displayName}</Card.Header>
                        <Card.Meta>
                            <span className='date'>Email: {userRef.email}</span>
                        </Card.Meta>
                        <Card.Description>Tham gia từ {userRef.metadata.creationTime}</Card.Description>
                    </Card.Content>
                    <Card.Content extra textAlign="center">
                        <a href="#">
                            <Icon name='user' />
                            Nhân viên
                        </a>
                    </Card.Content>
                </Card>
                 {/* <Modal trigger={<Button>Show Modal</Button>} centered={true}>
                    <Modal.Header>Select a Photo</Modal.Header>
                    <Modal.Content image>
                    <Image wrapped size='medium' src={userRef.photoURL} />
                    <Modal.Description>
                        <Header>Default Profile Image</Header>
                        <p>
                        We've found the following gravatar image associated with your e-mail
                        address.
                        </p>
                        <p>Is it okay to use this photo?</p>
                    </Modal.Description>
                    </Modal.Content>
                </Modal> */}
            </div>
        );
    }
}

export default UserProfile;