import React, { Component, createRef } from 'react';
import {Button, Icon, Menu, Segment,Sidebar, Sticky, Ref} from "semantic-ui-react";
import { Route, Link, BrowserRouter as Router } from "react-router-dom";
import UserProfile from './UserProfile';
import ChartPage from './ChartPage';
import App from '../App';
import MonthlyEnvironmentChart from './MonthlyEnvironmentChart';
import RegisterOrchard from './RegisterOrchard';
import DiseasesDetect from './DiseasesDetect';
import MapComponent from './MapComponent';
import DiseaseLibrary from './DiseaseLibrary';
import DiseaseDetails from './DiseaseDetails';
import firebase from '../../firebase';


class Dashboard extends Component {

    state = {
        visible: true
    }

    handleHideClick = () => this.setState({ visible: false })
    handleShowClick = () => this.setState({ visible: true })
    handleSidebarHide = () => this.setState({ visible: false })
    handleSignout = () => {
        firebase
          .auth()
          .signOut()
          .then(() => console.log("signed out!"));
      };


    render() {
        const {visible} = this.state;
        const contextRef = createRef();
        return (
            <Router>
            <React.Fragment>
                <Button.Group style={{marginLeft: "40%", marginTop: "10px"}}>
                    <Button color='black' disabled={visible} onClick={this.handleShowClick}>
                        Hiện thanh bên
                    </Button>
                    <Button color='black' disabled={!visible} onClick={this.handleHideClick}>
                        Ẩn thanh bên
                    </Button>
                   
                </Button.Group>
                
                <Ref innerRef={contextRef}>
                    
                    <Sidebar.Pushable as={Segment} style={{ transform: "none" }}>
                        <Sticky context={contextRef}>
                            <Sidebar 
                                as={Menu}
                                animation='overlay'
                                icon='labeled'
                                inverted
                                onHide={this.handleSidebarHide}
                                vertical
                                visible={visible}
                                width='thin'>
                                {/* <Menu.Item as={Link} to="/chat"><Icon name='rocketchat orange' />Chat</Menu.Item> */}
                                <Menu.Item as={Link} to ="/register-orchard"><Icon name='registered orange' />Đăng ký khu vườn</Menu.Item>
                                <Menu.Item as={Link} to="/profile"><Icon name='user orange' />Thông tin của tôi</Menu.Item>
                                <Menu.Item as={Link} to ="/export-data"><Icon name='database orange' />Xem/xuất dữ liệu</Menu.Item>
                                <Menu.Item as={Link} to ="/monthly-chart"><Icon name='chart line orange' />Biểu đồ thống kê tháng</Menu.Item>
                                <Menu.Item as={Link} to ="/diseases-detect"><Icon name='bug orange' />Tình trạng sâu bệnh</Menu.Item>
                                <Menu.Item as={Link} to ="/library"><Icon name='map orange' />Danh sách các loại bệnh trên cây trồng</Menu.Item>
                                {/* <Menu.Item as={Link} to ="/map"><Icon name='map marker alternate orange' />Bản đồ</Menu.Item> */}
                                {/* <Menu.Item as={Link} to ="/test"><Icon name='database orange' />Xem/xuất dữ liệu</Menu.Item> */}
                                <Menu.Item onClick={this.handleSignout}><Icon name='sign-out orange' />Đăng xuất</Menu.Item>
                            </Sidebar>
                        </Sticky>
                      
                        <Sidebar.Pusher>
                            {/* <Route exact path="/chat" component={App} /> */}
                            <Route exact path="/profile" component={UserProfile} />
                            <Route exact path="/export-data" component={ChartPage} />
                            <Route exact path="/monthly-chart" component={MonthlyEnvironmentChart} />
                            <Route exact path="/register-orchard" component={RegisterOrchard} />
                            <Route exact path="/diseases-detect" component={DiseasesDetect} />
                            <Route exact path="/library" component={DiseaseLibrary} />
                            <Route path="/library/:label" component={DiseaseDetails} /> 
                            {/* <Route exact path="/map" component={MapComponent} /> */}
                            {/* <Route exact path="/test" component={Test} /> */}
                        </Sidebar.Pusher>
                   
                    </Sidebar.Pushable>
                    
                </Ref>
             
                
            </React.Fragment>
            </Router>
        );
    }
}


export default Dashboard;