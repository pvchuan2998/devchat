import React, { Component } from 'react';
import { Button, Message, Segment, Card } from 'semantic-ui-react'
import Spreadsheet from "react-spreadsheet";
import axios from 'axios';
import DatePicker from "react-datepicker";
import jsPDF from "jspdf";
import "jspdf-autotable";
import { CSVLink } from "react-csv";
import "react-datepicker/dist/react-datepicker.css";



class LineChart extends Component {

    _isMounted = false;

    constructor(props){
        super(props)
        this.state = {
            startDate: new Date(),
            endDate: new Date(),
            data: [],
            loading: false
        };
        this.startDateChanged = this.startDateChanged.bind(this);
        this.clearStartDate = this.clearStartDate.bind(this);
        this.endDateChanged = this.endDateChanged.bind(this);
        this.clearEndDate = this.clearEndDate.bind(this);
        this.fetchData = this.fetchData.bind(this)
      }

    startDateChanged(d){
        this.setState({startDate: d});
        console.log(this.formatDate(this.state.startDate))
    }
    
    clearStartDate(){
        this.setState({startDate: null});
        console.log(this.formatDate(this.state.endDate))
    }

    endDateChanged(d){
        this.setState({endDate: d});
    }
    
    clearEndDate(){
        this.setState({endDate: null});
    }

   
    formatDate(date) {
        var d = new Date(date),
            month = '' + (d.getMonth() + 1),
            day = '' + d.getDate(),
            year = d.getFullYear();
    
        if (month.length < 2) 
            month = '0' + month;
        if (day.length < 2) 
            day = '0' + day;
    
        return [year, month, day].join('-');
    }


    exportPDF = () => {
        const unit = "pt";
        const size = "A4"; // Use A1, A2, A3 or A4
        const orientation = "portrait"; // portrait or landscape
    
        const marginLeft = 40;
        const doc = new jsPDF(orientation, unit, size);
    
        doc.setFontSize(12);
    
        const title = "Environmental data report";
        //const headers = [["NAME", "PROFESSION"]];
        const headers = [["Time","Humidity (%)", "Moisture (%)", "Temperature (oC)", "Soil temperature (oC)", "EC (mS/cm)", "PH"]];
    
        //const data = this.state.people.map(elt=> [elt.name, elt.profession]);
        const data = this.state.data.map(d => [(new Date(d.time._seconds * 1000).toDateString()), 
        d.airHumid, d.soilHumid, d.airTemp, d.soilTemp, d.ec, d.ph]);
    
        let content = {
          startY: 50,
          head: headers,
          body: data
        };
    
        doc.text(title, marginLeft, 30);
        doc.autoTable(content);
        doc.save("report.pdf")
      }

      exportFruitPDF = () => {
        const unit = "pt";
        const size = "A4"; // Use A1, A2, A3 or A4
        const orientation = "portrait"; // portrait or landscape
    
        const marginLeft = 40;
        const doc = new jsPDF(orientation, unit, size);
    
        doc.setFontSize(12);
    
        const title = "Fruit data report";
        //const headers = [["NAME", "PROFESSION"]];
        const headers = [["Time", "Fruit's name","Fruit's width (cm)", "Fruit's height (cm)"]];
    
        //const data = this.state.people.map(elt=> [elt.name, elt.profession]);
        const data = this.state.data.map(d => [(new Date(d.time._seconds * 1000).toDateString()), 
        d.fruit, d.width, d.height]);
    
        let content = {
          startY: 50,
          head: headers,
          body: data
        };
    
        doc.text(title, marginLeft, 30);
        doc.autoTable(content);
        doc.save("report.pdf")
      }


    componentDidUpdate(prevProps, prevState){
        if(this.props.currentOrchard !== prevProps.currentOrchard) {
            this.setState({data: null});
            this.setState({startDate: null, endDate: null});
            console.log(this.props.currentOrchard)
        }
    }

    componentWillUnmount() {
        this._isMounted = false;
    }

  
     fetchData(orchardId, start, end) {
        var url = `https://us-central1-graduationproject-4d612.cloudfunctions.net/getYearlyData/${orchardId}/${start}/${end}`
        this.setState({loading: true});
        axios.get(url)
            .then(res => {
                const data = res.data;
                this.setState({data});
                this.setState({loading: false});
                console.log(this.state.data)
               
            })
            .catch (error => {
                console.log("Error")
            });
    }

   
    render() {
        const {startDate, endDate, data, loading} = this.state
        const dataEnviExcelTemp = []
        if (data && data.length > 0) {
            for (let i=0; i< data.length; i++){
                dataEnviExcelTemp[i] = [
                {value: new Date(data[i].time._seconds * 1000).toDateString()},
                {value: data[i].airHumid}, 
                {value: data[i].airTemp}, 
                {value: data[i].soilHumid}, 
                {value: data[i].soilTemp}, 
                {value: data[i].ec}, {value: data[i].ph}]
            }
            console.log(dataEnviExcelTemp)
        }
        //only show the first 100 element in arr
        const dataEnviExcel = dataEnviExcelTemp && dataEnviExcelTemp.slice(0, 100)
        
        const dataFruitExcelTemp = data && data.map((data) => [
            {value:new Date(data.time._seconds * 1000).toDateString()},
            {value: data.fruit},
            {value: data.height},
            {value: data.width},
        ])
        //only show the first 100 element in arr
        const dataFruitExcel = dataFruitExcelTemp && dataFruitExcelTemp.slice(0, 100)

        const columnEnviLabels = ["Thời gian","Độ ẩm không khí (%)", "Nhiệt độ không khí (oC)",
             "Độ ẩm đất (%)", "Nhiệt độ đất (%)", "Độ EC đất (mS/cm)", "Độ PH đất"];

        const columnFruitLabels = ["Thời gian","Loại trái", "Chiều cao trái (cm)","Chiều rộng trái (cm)"];

        const timeArr = data && data.map((data) => new Date(data.time._seconds * 1000).toDateString())
        const latestUpdateTime = timeArr && timeArr[timeArr.length - 1]
        const dataEnviHeader = `Bảng dữ liệu môi trường - Cập nhật lần cuối: ${latestUpdateTime}`

        const dataFruitHeader = `Bảng dữ liệu trái cây - Cập nhật lần cuối: ${latestUpdateTime}`

        const csvHeaders = [
            { label: "Thời gian", key: "time" },
            { label: "Độ ẩm không khí (%)", key: "airHumid" },
            { label: "Độ ẩm đất (%)", key: "soilHumid" },
            { label: "Nhiệt độ không khí (oC)", key: "airTemp" },
            { label: "Nhiệt độ đất (oC)", key: "soilTemp" },
            { label: "EC (mS/cm)", key: "ec" },
            { label: "PH", key: "ph" },
          ];

        const csvFruitHeaders = [
        { label: "Thời gian", key: "time" },
        { label: "Loại trái", key: "fruit" },
        { label: "Chiều dài trái (cm)", key: "height" },
        { label: "Chiều rộng trái (cm)", key: "width" },
        ];

        return (
            <div style={{margin: "0 auto", display: "table", width: "65%"}}>
                <div style={{display:"flex", flexDirection:"column", justifyContent:"center", alignItems:"center"}}>
                    <div >
                        <span style={{fontFamily: "Arial", fontSize:"20px", marginRight:"10px"}}>Ngày bắt đầu:</span>

                        <DatePicker style={{fontSize:"20px"}} selected={this.state.startDate} onChange={this.startDateChanged} />

                        <input style={{marginLeft:"10px"}} className="ui inverted orange small button" type="button" onClick={this.clearStartDate} value="Xóa"/>
                    </div>

                    <div style={{marginTop: "15px"}}>
                        <span style={{fontFamily: "Arial", fontSize:"20px", marginRight:"10px"}}>Ngày kết thúc:</span>

                        <DatePicker  selected={this.state.endDate} onChange={this.endDateChanged} />

                        <input style={{marginLeft:"10px"}} className="ui inverted orange small button" type="button" onClick={this.clearEndDate} value="Xóa"/>
                    </div>

                    <Button style={{marginTop:"20px", marginBottom:"10px"}} size="large" color="orange" 
                        disabled={loading}
                        className={loading ? "loading" : ""} 
                        onClick={() => {this.fetchData(this.props.currentOrchard, this.formatDate(startDate), this.formatDate(endDate))}} 
                        >Xem dữ liệu</Button>
                </div>
                

                 <div>
                    {data && data.length > 0 ? 
                        <Segment loading={!data}>

                            <Card fluid color='orange' header={dataEnviHeader} />
                            <Spreadsheet data={dataEnviExcel} columnLabels={columnEnviLabels}/>
                            <div>
                                <Button onClick={() => this.exportPDF()} inverted color='orange' size='large' style={{marginTop: "10px"}}>
                                    Xuất PDF
                                </Button>

                                <CSVLink className="ui inverted orange large button"  filename={"report.csv"} data={data} headers={csvHeaders}>
                                    Xuất Excel
                                </CSVLink>
                            </div>
                            
                            <Card fluid color='orange' header={dataFruitHeader} />
                            <Spreadsheet data={dataFruitExcel} columnLabels={columnFruitLabels}/>
                            <div>
                                <Button onClick={() => this.exportFruitPDF()} inverted color='orange' size='large' style={{marginTop: "10px"}}>
                                    Xuất PDF
                                </Button>

                                <CSVLink className="ui inverted orange large button"  filename={"report.csv"} data={data} headers={csvFruitHeaders}>
                                    Xuất Excel
                                </CSVLink>
                            </div>

                           
                        </Segment>
                        
                    : 
                        <Message style={{marginTop:"75px"}} className="container center aligned">
                            <Message.Header>Thông báo!</Message.Header>
                            <p>
                                Không có dữ liệu!
                            </p>
                        </Message>
                    
                    }
                 </div>
                
            </div>
        );
    }
}

export default LineChart;