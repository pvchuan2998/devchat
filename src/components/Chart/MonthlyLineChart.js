import React, { Component } from 'react';
import {Line} from 'react-chartjs-2';
import firebase from '../../firebase';
import { Card, Message, Button } from 'semantic-ui-react'
import jsPDF from "jspdf";
import "jspdf-autotable";
import { CSVLink } from "react-csv";
import moment from 'moment';
//import html2canvas from "html2canvas";
import domtoimage from 'dom-to-image';
import { saveAs } from 'file-saver';
//const pdfConverter = require("jspdf");
import Spreadsheet from "react-spreadsheet";



class MonthlyLineChart extends Component {

    _isMounted = false;

    state = {
        data: [],
        // dumbData:[],
        averageAirTemp: null,
        averageAirHumid: null,
        averageSoilTemp: null,
        averageSoilHumid: null,
        averageEC: null,
        averagePH: null,

        minAirTemp: null,
        maxAirTemp: null,

        minSoilTemp: null,
        maxSoilTemp: null,

        minAirHumid:null,
        maxAirHumid: null,

        minSoilHumid: null,
        maxSoilHumid: null,
        minEC: null,
        maxEC: null,

        minPH:null,
        maxPH: null
    }

    exportPDF = () => {
        const unit = "pt";
        const size = "A4"; // Use A1, A2, A3 or A4
        const orientation = "portrait"; // portrait or landscape
    
        const marginLeft = 40;
        const doc = new jsPDF(orientation, unit, size);
    
        doc.setFontSize(12);
    
        const title = "Environmental data report";
        //const headers = [["NAME", "PROFESSION"]];
        const headers = [["Time","Humidity (%)", "Moisture (%)", "Temperature (oC)", "Soil temperature (oC)",
             "EC (mS/cm)", "PH", "Fruit", "Fruit's Height (cm)", "Fruit's Width (cm)"]];
    
        //const data = this.state.people.map(elt=> [elt.name, elt.profession]);
        const data = this.state.data.map(d=> [moment(d.thoiGianGui && d.thoiGianGui.toDate()).format('MMMM Do YYYY, h:mm:ss a'), 
        d.doAmKK, d.doAmDat, d.nhietDoKK, d.nhietDoDat, d.doECDat, d.doPHDat, d.loaiTrai, d.kichThuocTrai_Dai, d.kichThuocTrai_Rong]);
    
        let content = {
          startY: 50,
          head: headers,
          body: data
        };
    
        doc.text(title, marginLeft, 30);
        doc.autoTable(content);
        doc.save("report.pdf")
      }


    fetchMonthlyData = (orchardId, month) => {
        const db = firebase.firestore()
        // let start = new Date('2020-07-11')
        // let end = new Date('2020-07-14')
        let start 
        let end 
        //const monthlyData 
        if(month === "1"){
            start = new Date('2020-01-01')
            end = new Date('2020-01-31')
        }
        if(month === "2"){
            start = new Date('2020-02-01')
            end = new Date('2020-02-29')
        }
        if(month === "3"){
            start = new Date('2020-03-01')
            end = new Date('2020-03-31')
        }
        if(month === "4"){
            start = new Date('2020-04-01')
            end = new Date('2020-04-30')
        }
        if(month === "5"){
            start = new Date('2020-05-01')
            end = new Date('2020-05-31')
        }
        if(month === "6"){
            start = new Date('2020-06-01')
            end = new Date('2020-06-30')
        }
        if(month === "7"){
            start = new Date('2020-07-01')
            end = new Date('2020-07-31')
        }
        if(month === "8"){
            start = new Date('2020-08-01')
            end = new Date('2020-08-31')
        }
        if(month === "9"){
            start = new Date('2020-09-01')
            end = new Date('2020-09-30')
        }
        if(month === "10"){
            start = new Date('2020-10-01')
            end = new Date('2020-10-31')
        }
        if(month === "11"){
            start = new Date('2020-11-01')
            end = new Date('2020-11-30')
        }
        if(month === "12"){
            start = new Date('2020-12-01')
            end = new Date('2020-12-31')
        }

        db.collection("KetQuaDo")
            .where('maSoKhuVuon', '==', orchardId)
            .where('thoiGianGui', '>=', start)
            .where('thoiGianGui', '<=', end)
            .get()
            .then(querySnapshot => {
                const data = querySnapshot.docs.map(doc => doc.data());
                const airTemp = data.map((data => data.nhietDoKK))
                const airHumid = data.map((data => data.doAmKK))
                const soilTemp = data.map((data => data.nhietDoDat))
                const soilHumid = data.map((data => data.doAmDat))
                const ec = data.map((data => data.doECDat))
                const ph = data.map((data => data.doPHDat))
                 if(this._isMounted){
                    this.setState({ data: data});
                    this.setState({averageAirTemp: this.average(airTemp)});
                    this.setState({averageAirHumid: this.average(airHumid)});
                    this.setState({averageSoilTemp: this.average(soilTemp)});
                    this.setState({averageSoilHumid: this.average(soilHumid)});
                    this.setState({averageEC: this.average(ec)});
                    this.setState({averagePH: this.average(ph)});

                    this.setState({minAirTemp: Math.min(...airTemp)});
                    this.setState({maxAirTemp: Math.max(...airTemp)});

                    this.setState({maxAirHumid: Math.max(...airHumid)});
                    this.setState({minAirHumid: Math.min(...airHumid)});

                    this.setState({maxSoilHumid: Math.max(...soilHumid)});
                    this.setState({minSoilHumid: Math.min(...soilHumid)});

                    this.setState({minSoilTemp: Math.min(...soilTemp)});
                    this.setState({maxSoilTemp: Math.max(...soilTemp)});

                    this.setState({minEC: Math.min(...ec)});
                    this.setState({maxEC: Math.max(...ec)});

                    this.setState({minPH: Math.min(...ph)});
                    this.setState({maxPH: Math.max(...ph)});
                }
            });   
    }

    average(arr) {
        return arr.reduce((a, b) => (a + b), 0) / arr.length;
    }

  
    componentDidMount() {
        this._isMounted = true;
        this.fetchMonthlyData(this.props.currentOrchardName, this.props.currentMonth)
       
    }
    

    componentDidUpdate(prevProps) {
        // if((this.props.currentOchardName !== prevProps.currentOchardName) 
        //     || (this.props.currentMonth !== prevProps.currentMonth))
        if(this.props.currentOrchardName !== prevProps.currentOrchardName)
        {
            this.fetchMonthlyData(this.props.currentOrchardName, this.props.currentMonth)
            console.log(this.props.currentOrchardName)
            // console.log(this.props.currentMonth)
        }

        if(this.props.currentMonth !== prevProps.currentMonth){
            this.fetchMonthlyData(this.props.currentOrchardName, this.props.currentMonth)
            console.log(this.props.currentMonth)
        }
    }

    componentWillUnmount() {
        this._isMounted = false;
    }

    exportChart2Pdf(id) {
        //const input = document.getElementById('divToOfferInfo');
        //const pdf = new jsPDF();
        const input = document.getElementById(id);
        const pdf = new jsPDF({
            orientation: 'landscape',
          });
          
        if (pdf) {
          domtoimage.toPng(input)
            .then(imgData => {
            const imgProps= pdf.getImageProperties(imgData);
            const pdfWidth = pdf.internal.pageSize.getWidth();
            const pdfHeight = (imgProps.height * pdfWidth) / imgProps.width;
            //pdf.addImage(imgData, 'PNG', 10, 10);
            pdf.addImage(imgData, 'PNG', 0, 0, pdfWidth, pdfHeight);
            pdf.save('chart.pdf');
            });
        }
    }

    exportPNG(id) {
        domtoimage.toBlob(document.getElementById(id))
            .then(function (blob) {
                saveAs(blob, 'chart.png');
        });
    }
  
    render() {
        const {data, 
            averageAirTemp,
            averageAirHumid,
            averageSoilTemp,
            averageSoilHumid,
            averageEC,
            averagePH,
            minAirTemp, maxAirTemp, minAirHumid, maxAirHumid, minSoilTemp, maxSoilTemp,
            minSoilHumid, maxSoilHumid, minEC, maxEC, minPH, maxPH} = this.state
        
        const csvHeaders = [
            { label: "Time", key: "thoiGianGui" },
            { label: "Humidity (%)", key: "doAmKK" },
            { label: "Moisture (%)", key: "doAmDat" },
            { label: "AirTemperature (oC)", key: "nhietDoKK" },
            { label: "SoidTemperature (oC)", key: "nhietDoDat" },
            { label: "EC (mS/cm)", key: "doECDat" },
            { label: "PH", key: "doPHDat" },
            { label: "Fruit", key: "loaiTrai" },
            { label: "Fruit's width (cm)", key: "kichThuocTrai_Rong" },
            { label: "Fruit's height (cm)", key: "kichThuocTrai_Dai" }
          ];

          const dataCsv = data.map((data) => [
            {thoiGianGui: moment(data.thoiGianGui && data.thoiGianGui.toDate()).format('MMMM Do YYYY, h:mm:ss a'),
            doAmKK: data.doAmKK,
            doAmDat: data.doAmDat,
            nhietDoKK: data.nhietDoKK,
            nhietDoDat: data.nhietDoDat,
            doECDat: data.doECDat,
            doPHDat: data.doPHDat,
            loaiTrai: data.loaiTrai,
            kichThuocTrai_Dai: data.kichThuocTrai_Dai,
            kichThuocTrai_Rong: data.kichThuocTrai_Rong}
          ])
           
          const headerAirTemp = `Nhiệt độ không khí tháng ${this.props.currentMonth} - Trung bình: ${averageAirTemp} (oC) - Thấp nhất: ${minAirTemp} (oC) - Cao nhất: ${maxAirTemp} (oC)`
          const headerAirHumid = `Độ ẩm không khí tháng ${this.props.currentMonth} - Trung bình: ${averageAirHumid} (%) - Thấp nhất: ${minAirHumid} (%) - Cao nhất: ${maxAirHumid} (%)`
          const headerSoilTemp = `Nhiệt độ đất tháng ${this.props.currentMonth} - Trung bình: ${averageSoilTemp} (oC) - Thấp nhất: ${minSoilTemp} (oC) - Cao nhất: ${maxSoilTemp} (oC)`
          const headerSoilHumid = `Độ ẩm đất tháng ${this.props.currentMonth} - Trung bình: ${averageSoilHumid} (%) - Thấp nhất: ${minSoilHumid} (%) - Cao nhất: ${maxSoilHumid} (%)`
          const headerEC = `Độ dẫn điện trong đất tháng ${this.props.currentMonth} - Trung bình: ${averageEC} (mS/cm) - Thấp nhất: ${minEC} (mS/cm) - Cao nhất: ${maxEC} (mS/cm)`
          const headerPH = `Độ pH trong đất tháng ${this.props.currentMonth} - Trung bình: ${averagePH} - Thấp nhất: ${minPH} - Cao nhất: ${maxPH} `
          const dataTest = (canvas) => {
            const ctx = canvas.getContext("2d");
            const gradient = ctx.createLinearGradient(0,0,0,500);
            gradient.addColorStop(0, "rgba(139, 223, 227, 1)");   
            gradient.addColorStop(1, "rgba(139, 223, 227, 0)");

            const ctx1 = canvas.getContext("2d");
            const gradient1 = ctx1.createLinearGradient(0,0,0,500);
            gradient1.addColorStop(0, 'rgba(250,174,50,1)');   
            gradient1.addColorStop(1, 'rgba(250,174,50,0)');
    
            return {
                labels: data.map(d => moment(d.thoiGianGui && d.thoiGianGui.toDate()).format("MMMM Do YYYY, h:mm:ss a")),
                datasets: [
                    {
                        backgroundColor : gradient, // Put the gradient here as a fill color
                        label: 'Nhiệt độ không khí(oC)',
                        borderColor: 'rgba(0,0,0,1)',
                        borderWidth: 2,
                        lineTension: 0,
                        pointColor : "#fff",
                        pointStrokeColor : "#ff6c23",
                        pointHighlightFill: "#fff",
                        pointHighlightStroke: "#ff6c23",
                        data : data.map(data => data.nhietDoKK)
                    },
                    {
                        data: data.map(data => data.nhietDoDat),
                        label: 'Nhiệt độ đất(oC)',
                        backgroundColor : gradient1,
                        borderColor: 'rgba(0,0,0,1)',
                        borderWidth: 2,
                        lineTension: 0
                    }
                ]
            }
        }

        const dataTest1 = (canvas) => {
            const ctx = canvas.getContext("2d");
            const gradient = ctx.createLinearGradient(0,0,0,500);
            gradient.addColorStop(0, "rgba(139, 223, 227, 1)");   
            gradient.addColorStop(1, "rgba(139, 223, 227, 0)");

            const ctx1 = canvas.getContext("2d");
            const gradient1 = ctx1.createLinearGradient(0,0,0,500);
            gradient1.addColorStop(0, 'rgba(250,174,50,1)');   
            gradient1.addColorStop(1, 'rgba(250,174,50,0)');
    
            return {
                labels: data.map(d => moment(d.thoiGianGui && d.thoiGianGui.toDate()).format("MMMM Do YYYY, h:mm:ss a")),
                datasets: [
                    {
                        backgroundColor : gradient, // Put the gradient here as a fill color
                        label: 'Độ ẩm không khí(%)',
                        borderColor: 'rgba(0,0,0,1)',
                        borderWidth: 2,
                        lineTension: 0,
                        pointColor : "#fff",
                        pointStrokeColor : "#ff6c23",
                        pointHighlightFill: "#fff",
                        pointHighlightStroke: "#ff6c23",
                        data : data.map(data => data.doAmKK)
                    },
                    {
                        data: data.map(data => data.doAmDat),
                        label: 'Độ ẩm đất(%)',
                        backgroundColor : gradient1,
                        borderColor: 'rgba(0,0,0,1)',
                        borderWidth: 2,
                        lineTension: 0
                    }
                ]
            }
        }

        const dataTest2 = (canvas) => {
            const ctx = canvas.getContext("2d");
            const gradient = ctx.createLinearGradient(0,0,0,500);
            gradient.addColorStop(0, "rgba(139, 223, 227, 1)");   
            gradient.addColorStop(1, "rgba(139, 223, 227, 0)");

            return {
                labels: data.map(d => moment(d.thoiGianGui && d.thoiGianGui.toDate()).format("MMMM Do YYYY, h:mm:ss a")),
                datasets: [
                    {
                        backgroundColor : gradient, // Put the gradient here as a fill color
                        label: 'Độ đẫn điện EC (mS/cm)',
                        borderColor: 'rgba(0,0,0,1)',
                        borderWidth: 2,
                        lineTension: 0,
                        pointColor : "#fff",
                        pointStrokeColor : "#ff6c23",
                        pointHighlightFill: "#fff",
                        pointHighlightStroke: "#ff6c23",
                        data : data.map(data => data.doECDat)
                    }
                ]
            }
        }

        const dataTest3 = (canvas) => {
            const ctx = canvas.getContext("2d");
            const gradient = ctx.createLinearGradient(0,0,0,500);
            gradient.addColorStop(0, "rgba(139, 223, 227, 1)");   
            gradient.addColorStop(1, "rgba(139, 223, 227, 0)");

            return {
                labels: data.map(d => moment(d.thoiGianGui && d.thoiGianGui.toDate()).format("MMMM Do YYYY, h:mm:ss a")),
                datasets: [
                    {
                        backgroundColor : gradient, // Put the gradient here as a fill color
                        label: 'Độ pH',
                        borderColor: 'rgba(0,0,0,1)',
                        borderWidth: 2,
                        lineTension: 0,
                        pointColor : "#fff",
                        pointStrokeColor : "#ff6c23",
                        pointHighlightFill: "#fff",
                        pointHighlightStroke: "#ff6c23",
                        data : data.map(data => data.doPHDat)
                    }
                ]
            }
        }

        const dataEnviExcel = data.map((data) => [
            {value: moment(data.thoiGianGui && data.thoiGianGui.toDate()).format('MMMM Do YYYY, h:mm:ss a')},
            {value: data.doAmKK},
            {value: data.nhietDoKK},
            {value: data.doAmDat},
            {value: data.nhietDoDat},
            {value: data.doECDat},
            {value: data.doPHDat},
        ])
        const dataFruitExcelTemp = data.map((data) => [
            {value: moment(data.thoiGianGui && data.thoiGianGui.toDate()).format('MMMM Do YYYY, h:mm:ss a')},
            {value: data.loaiTrai},
            {value: data.kichThuocTrai_Dai},
            {value: data.kichThuocTrai_Rong},
        ])
        const dataFruitExcel = dataFruitExcelTemp.slice(-100)


        const columnEnviLabels = ["Thời gian","Độ ẩm không khí (%)", "Nhiệt độ không khí (oC)", "Độ ẩm đất (%)", "Nhiệt độ đất (oC)", "Độ EC đất (mS/cm)", "Độ PH đất"];
        const columnFruitLabels = ["Thời gian","Loại trái", "Chiều cao trái (cm)","Chiều rộng trái (cm)"];
        const timeArr = data.map((data) => moment(data.thoiGianGui && data.thoiGianGui.toDate()).format('MMMM Do YYYY, h:mm:ss a'))
        const latestUpdateTime = timeArr[timeArr.length - 1]
        const dataEnviHeader = `Bảng dữ liệu môi trường - Cập nhật lần cuối: ${latestUpdateTime}`
        const dataFruitHeader = `Bảng dữ liệu trái cây trong tháng ${this.props.currentMonth} - Cập nhật lần cuối: ${latestUpdateTime}`
        
        return (
            data.length > 0 ? (
                <div style={{margin: "0 auto", display: "table", width: "70%"}}>
                   
                    {/* <Card fluid color='red' header={dataEnviHeader} />
                    <Spreadsheet data={dataEnviExcel} columnLabels={columnEnviLabels}/> */}

                    <Card fluid color='red' header={dataFruitHeader} />
                    <Spreadsheet data={dataFruitExcel} columnLabels={columnFruitLabels}/>

                    <div>
                        <Button onClick={() => this.exportPDF()} inverted color='orange' size='large' style={{marginBottom: "10px"}}>
                            Xuất PDF
                        </Button>

                        <CSVLink className="ui inverted orange large button"  filename={"report.csv"} data={data} headers={csvHeaders}>
                            Xuất Excel
                        </CSVLink>
                    </div>
                 <div style={{marginTop: "25px"}} id="div1">
                    <Card.Group >
                        <Card.Header style={{display: 'flex',justifyContent: 'space-between', alignItems: 'center', width:"100%"}}>
                            <Card fluid color='red' header={headerAirTemp} />
                            <Card style={{top:"-14px"}} fluid color='red' header={headerSoilTemp} />
                        </Card.Header>
                            <Line 
                                data={dataTest}
                                options={{
                                    legend:{
                                        display:true,
                                        position:"right"
                                    },
                                    
                                }}
                            />

                        <div>
                            <Button style={{fontFamily: "Arial"}} inverted color='orange' onClick={() => this.exportChart2Pdf('div1')}>Xuất biểu đồ PDF</Button>
                            <Button style={{fontFamily: "Arial"}} inverted color='orange' onClick={() => this.exportPNG('div1')}>Xuất biểu đồ PNG</Button>
                        </div>
                    </Card.Group>
                    
                </div>

                <div style={{marginTop: "25px"}} id="div2">
                    <Card.Group>
                    <Card.Header style={{display: 'flex',justifyContent: 'space-between', alignItems: 'center', width:"100%"}}>
                        <Card fluid color='red' header={headerAirHumid} />
                        <Card style={{top:"-14px"}} fluid color='red' header={headerSoilHumid} />
                    </Card.Header>
                        
                        <Line 
                            data={dataTest1}
                            options={{
                                legend:{
                                    display:true,
                                    position:"right"
                                }
                            }}
                        /> 
                        <div >
                            <Button style={{fontFamily: "Arial"}} inverted color='orange' onClick={() => this.exportChart2Pdf('div2')}>Xuất biểu đồ PDF</Button>
                            <Button style={{fontFamily: "Arial"}} inverted color='orange' onClick={() => this.exportPNG('div2')}>Xuất biểu đồ PNG</Button>
                        </div> 
                    </Card.Group>
                </div>

               
                <div style={{marginTop: "25px"}} id="div3">
                    <Card.Group>
                        <Card fluid color='red' header={headerEC} />
                        <Line 
                            data={dataTest2}
                            options={{
                                legend:{
                                    display:true,
                                    position:"right"
                                }
                            }}
                        /> 
                         <div>
                            <Button style={{fontFamily: "Arial"}} inverted color='orange' onClick={() => this.exportChart2Pdf('div3')}>Xuất biểu đồ PDF</Button>
                            <Button style={{fontFamily: "Arial"}} inverted color='orange' onClick={() => this.exportPNG('div3')}>Xuất biểu đồ PNG</Button>
                        </div>
                    </Card.Group>
                </div>

                <div style={{marginTop: "25px", marginBottom: "25px"}} id= "div4">
                    <Card.Group>
                        <Card fluid color='red' header={headerPH} />
                        <Line 
                            data = {dataTest3}
                            options={{
                                legend:{
                                    display:true,
                                    position:"right"
                                }
                            }}
                        /> 
                        <div>
                            <Button style={{fontFamily: "Arial"}} inverted color='orange' onClick={() => this.exportChart2Pdf('div4')}>Xuất biểu đồ PDF</Button>
                            <Button style={{fontFamily: "Arial"}} inverted color='orange' onClick={() => this.exportPNG('div4')}>Xuất biểu đồ PNG</Button>
                        </div>
                    </Card.Group>
                </div>
                
            </div>
            ) : (
                <Message className="container center aligned">
                <Message.Header>Thông báo!</Message.Header>
                    <p>
                        Không có dữ liệu!
                    </p>
              </Message>
            )
            
        );
    }
}

export default MonthlyLineChart;