import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { Button} from "semantic-ui-react";


class NotFoundPage extends Component {

    
    render() {
       
        return (
            <div style={{marginTop:70}} width="100%">
                <div style={{margin:"0 auto", width:"75%"}}>
                    <img width="1000px" height="500px" src='https://wpklik.com/wp-content/uploads/2019/03/A-404-Page-Best-Practices-and-Design-Inspiration.jpg' alt="pagenotfound" /> 
                    <div style={{textAlign:"center", marginTop:20}}>
                        <Button size="big" color='blue'>
                            <Link style={{color: "black", fontFamily:"Arial", fontSize:"18px"}} to="/"> Quay lại </Link>
                        </Button>
                    </div>
                    
                </div>
            </div>
        );
    }
}

export default (NotFoundPage);