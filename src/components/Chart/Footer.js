import React, { Component } from 'react';
import {
    Container,
    Grid,
    Header,
    Image,
    List,
    Segment
  } from 'semantic-ui-react'

class Footer extends Component {
    render() {
        return (
            <Segment color="blue" inverted vertical style={{ padding: '3em 0em', fontFamily: "Arial" }}>
            <Container>
              <Grid divided inverted stackable>
                <Grid.Row>
                  <Grid.Column width={3}>
                    <Header inverted as='h4' content='Các tính năng chính' />
                    <List link inverted>
                      <List.Item as='a'>Thu thập thông tin môi trường và trái</List.Item>
                      <List.Item as='a'>Biểu đồ thống kê</List.Item>
                      <List.Item as='a'>Nhận dạng sâu bệnh</List.Item>
                    </List>
                  </Grid.Column>
                  <Grid.Column width={3}>
                    <Header inverted as='h4' content='Đội ngũ phát triển' />
                    <List link inverted>
                      <List.Item as='a'>Phạm Văn Chuẩn</List.Item>
                      <List.Item as='a'>Nguyễn Ảnh Đạt</List.Item>
                      <List.Item as='a'>Phan Trần Thế Duy</List.Item>
                      <List.Item as='a'>Trần Quang Hưng</List.Item>
                    </List>
                  </Grid.Column>
                  <Grid.Column width={7} style={{marginLeft: "80px"}}>
                    <Header as='h4' inverted>
                    <Image size='small'  style={{marginRight: "10px"}} src ='https://image.flaticon.com/icons/svg/1555/1555549.svg'/> 
                        AgriCollect
                    <Image style={{marginLeft: "10px"}} src ='https://nhanlucnganhluat.vn/uploads/images/9426F66C/logo/2019-03/35228463_2076230712644398_4635641207110762496_n.png'/> 
                  
                    </Header>
                    <p>
                      Ứng dụng hỗ trợ thu thập thông tin nông nghiệp. Được phát triển bởi các sinh viên đến từ Trường Đại học Khoa học Tự nhiên TP. Hồ Chí Minh
                    </p>
                    <p>
                      227 Nguyễn Văn Cừ, P. 4, Q. 5, TP. Hồ Chí Minh
                    </p>
                  </Grid.Column>
                </Grid.Row>
              </Grid>
            </Container>
          </Segment>
        );
    }
}

export default Footer;