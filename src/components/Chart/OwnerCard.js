import React, { Component } from 'react';
import firebase from '../../firebase';
import {Card} from "semantic-ui-react";

class OwnerCard extends Component {

    _isMounted = false;

    state = {
        currentOwnerData: {}
    }

    fetchOwnerData = (id) => {
        const db = firebase.firestore()

        if(id === ""){
            db.collection("Owner")
            .doc("No")
            .get()
            .then(doc => {
                const data = doc.data();
                
                if(this._isMounted){
                    this.setState({ currentOwnerData: data});
                }
                
                console.log(this.state.currentOwnerData);
            
            });  
        }
        else{
            db.collection("Owner")
            .doc(id)
            .get()
            .then(doc => {
                const data = doc.data();
                
                if(this._isMounted){
                    this.setState({ currentOwnerData: data});
                }
                
                console.log(this.state.currentOwnerData);
            
            });  
        }
    }

    // componentDidMount() {

    //     this._isMounted = true;

    //     this.fetchOwnerData(this.props.currentOwnerId);
    // }

    componentDidUpdate(prevProps, prevState) {
        this._isMounted = true;
        if(this.props.currentOwnerId !== prevProps.currentOwnerId) {
            this.fetchOwnerData(this.props.currentOwnerId)
        }
    }
    
    
    
    componentWillUnmount() {
        this._isMounted = false;
      }

    render() {
        const {currentOwnerData} = this.state;
        return (
            currentOwnerData ? (<Card style={{width: "40%", margin: "10px auto"}}>
                <Card.Content>
                    <Card.Header>Thông tin chủ vườn</Card.Header>
                    <Card.Meta>Owner</Card.Meta>
                    <Card.Description>
                        Tên chủ vườn: <span style={{color: 'red'}}>{currentOwnerData.name}</span>
                    </Card.Description>
                    <Card.Description>
                        Số điện thoại: <span style={{color: 'red'}}>{currentOwnerData.phone}</span>
                    </Card.Description>
                    <Card.Description>
                        Địa chỉ: <span style={{color: 'red'}}>{currentOwnerData.address}</span>
                    </Card.Description>
                </Card.Content>
            </Card>) : (
                <Card style={{width: "40%", margin: "10px auto"}}>
                <Card.Content
                  header='Thông tin chủ vườn chưa cập nhật!'
                  meta='No data'
                  description='No data'
                />
              </Card>
            )
        );
    }
}

export default OwnerCard;