import React, { Component } from 'react';
import {Card} from "semantic-ui-react";

class OrchardInfo extends Component {
    render() {
        return (
            <Card style={{width: "60%", margin: "10px auto"}}>
                <Card.Content>
                    <Card.Header style={{display: 'flex',justifyContent: 'space-between', alignItems: 'center'}}>
                        <div>{this.props.currentOrchardData.address}</div> 
                        <div>Diện tích : {this.props.currentOrchardData.area}(ha)</div>
                    </Card.Header>
                    <Card.Meta style={{display: 'flex',justifyContent: 'space-between', alignItems: 'center'}}>
                        <p>
                            Loại đất canh tác: <span style={{color: 'red'}}>{this.props.currentOrchardData.type}</span>
                        </p>
                    </Card.Meta>
                </Card.Content>
            </Card>
        );
    }
}

export default OrchardInfo;