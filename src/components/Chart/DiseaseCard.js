import React, { Component } from 'react';
import {Card, Message, Image, Item, Button, Icon} from 'semantic-ui-react';
import moment from 'moment';
import Carousel from 'react-elastic-carousel';
import { Link } from "react-router-dom";

class DiseaseCard extends Component {

    searchGoogle(textToSearch) {
        var res = encodeURIComponent(textToSearch);
        window.open("https://www.google.com/#q=" + res);
        //let win = window.open("//" + "google.com/search?q=" + textToSearch, '_blank');
    }


    renderCard = (data) => (
        data.map((data, key) => (
            
            <Card style={{width: "40%", margin: "5px auto", fontFamily: "Arial"}} key={key}>
                <Card.Content>
                    <Image
                        floated='right'
                        size='medium'
                        src={data.imgUri}
                    />
                    <Card.Header>Tình trạng cây trồng</Card.Header>
                    <Card.Meta>Loại cây trồng: <span style={{color:"red"}}>{data.plant}</span></Card.Meta>
                    <Card.Description>
                        Loại sâu bệnh có thể đang gặp phải: <span title="Nhấn vào để tìm kiếm Google" 
                                                             onClick={() => this.searchGoogle(data.diagnosedDisease)} 
                                                             style={{color:"red", cursor:"pointer"}}>{data.diagnosedDisease}</span>
                    </Card.Description>
                    <Button size="mini" inverted color='orange' icon labelPosition='right'>
                        <Link style={{color: "black", fontFamily:"Arial", fontSize:"14px"}} to={`/library/${data.diagnosedDisease.replace(/ /g,'')}`}> Xem chi tiết về bệnh </Link>
                        <Icon name='right arrow' />
                    </Button>
                    <Card.Description>
                        Thời gian: <span style={{color:"red"}}> {moment( data.time && data.time.toDate()).format('MMMM Do YYYY, h:mm:ss a')}</span>
                    </Card.Description>
                    
                    <Card.Description>
                        Diện tích vùng bị nhiễm: <span style={{color:"red"}}>{data.infectedArea} (m2)</span>
                    </Card.Description>
                    <Card.Description>
                        Hình ảnh vùng bị nhiễm:
                        {/* <Image
                            floated='right'
                            size='medium'
                            src={data.infectedAreaImgUri}
                        /> */}
                        <Carousel itemsToShow={1}>
                            <Item>
                                <Image
                                    floated='right'
                                    size='medium'
                                    src={data.infectedAreaImgUri}
                                />
                            </Item>
                            <Item>
                                <Image
                                    floated='right'
                                    size='medium'
                                    src={data.imgUri}
                                />
                            </Item>
                            
                        </Carousel>
                    </Card.Description> 
                    {/* <Carousel itemsToShow={1}>
                        <Item>
                            <Image
                                floated='right'
                                size='medium'
                                src={data.imgUri}
                            />
                        </Item>
                        <Item>
                            <Image
                                floated='right'
                                size='medium'
                                src={data.infectedAreaImgUri}
                            />
                        </Item>
                    </Carousel> */}
                </Card.Content>
            </Card>
           
        ))
    )

    render() {
        const {data} = this.props
        return (
           data.length > 0 ? (
               this.renderCard(data)
           ) : (
            <Message className="container center aligned">
                <Message.Header>Thông báo!</Message.Header>
                <p>
                    Không có dữ liệu!
                </p>
            </Message>
           )
        );
    }
}

export default DiseaseCard;