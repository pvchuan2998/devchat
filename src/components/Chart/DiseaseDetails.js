import React, { Component } from 'react';
import {Icon, Grid, Accordion, Header, Image, Breadcrumb} from 'semantic-ui-react';
import firebase from '../../firebase';
import {Link} from "react-router-dom";

class DiseaseDetails extends Component {

    _isMounted = false;

    state = { 
        activeIndexs: [],
        data: {}
    };

    handleClick = (e, titleProps) => {
        const { index } = titleProps;
        const { activeIndexs } = this.state;
        const newIndex = activeIndexs;
    
        const currentIndexPosition = activeIndexs.indexOf(index);
        if (currentIndexPosition > -1) {
          newIndex.splice(currentIndexPosition, 1);
        } else {
          newIndex.push(index);
        }
    
        this.setState({ activeIndexs: newIndex });
      };


    componentDidMount() {
        
        this._isMounted = true;
        const label = this.props.match.params.label;
        console.log(label)
        const db = firebase.firestore()

        db.collection("DiseaseDetails")
        .doc(label)
        .get()
        .then(doc => {
            const data = doc.data();
            
            if(this._isMounted){
                this.setState({data});
                console.log(this.state.data)
            }
            
        });

        // (async () => {
        //     const doc = await ref.get();
        //     if (!doc.exists) {
        //         console.log('No such document!');
        //     } else {
        //         if(this._isMounted){
        //             this.setState({data});
        //             console.log(data)
        //         }
        //         console.log('Document data:', doc.data());
        //     }  
        // })();

    }

    
    
    componentWillUnmount() {
        this._isMounted = false;
    }


    render() {

        const { activeIndexs, data} = this.state;
      
        return (
         
            <div style={{width:"100%"}}>
                {/* <Breadcrumb size="massive" style={{fontFamily:"Arial", margin:20}}>
                    <Breadcrumb.Section link><Link to="/">Trang chủ</Link></Breadcrumb.Section>
                    <Breadcrumb.Divider icon='right chevron' />
                    <Breadcrumb.Section link><Link to="/library">Thư viện</Link></Breadcrumb.Section>
                    <Breadcrumb.Divider icon='right arrow' />
                    <Breadcrumb.Section active style={{fontFamily:"Arial"}}>{data && data.vnName}</Breadcrumb.Section>
                </Breadcrumb> */}
                <Grid centered columns={2} style={{ margin:"0 auto"}}>
                
                <img alt="diseaseimage" style={{border:"2px solid #ddd", borderRadius:"7px", padding:"10px", marginTop:20
                , boxShadow:"0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19)"}} 
                    width="400" height="400" src={data.imageUrl}
                 />
                
                </Grid>
                <div style={{width:"70%", margin:"0 auto"}}>
                
                <Header textAlign='center' as='h2' block style={{marginTop:20}}>
                    <Image circular src={data.imageUrl} />
                    {data.name} - {data.vnName}
                </Header>
                <Accordion styled style={{width:"100%"}}>
                    <Accordion.Title
                        active={activeIndexs.includes(0)}
                        index={0}
                        onClick={this.handleClick}
                        style={{fontSize: 24, fontFamily: "Arial"}}
                    >
                        <Icon name="dropdown" />
                        Tổng quan
                    </Accordion.Title>
                    <Accordion.Content active={activeIndexs.includes(0)} >
                        <p style={{fontSize: 18, fontFamily: "Arial", textAlign:"justify"}}>
                           {data.causes}
                        </p>
                    </Accordion.Content>

                    <Accordion.Title
                        active={activeIndexs.includes(1)}
                        index={1}
                        onClick={this.handleClick}
                        style={{fontSize: 24, fontFamily: "Arial"}}
                        >
                        <Icon name="dropdown" />
                        Vật chủ
                    </Accordion.Title>
                    <Accordion.Content active={activeIndexs.includes(1)} >
                        <p style={{fontSize: 18, fontFamily: "Arial"}}>
                            {data.host}
                        </p>
                    </Accordion.Content>

                    <Accordion.Title
                        active={activeIndexs.includes(2)}
                        index={2}
                        onClick={this.handleClick}
                        style={{fontSize: 24, fontFamily: "Arial"}}
                        >
                        <Icon name="dropdown" />
                        
                        Triệu chứng
                    </Accordion.Title>
                    <Accordion.Content active={activeIndexs.includes(2)} >
                        <p style={{fontSize: 18, fontFamily: "Arial", textAlign:"justify"}}>
                            {data.details}
                        </p>
                    </Accordion.Content>

                    <Accordion.Title
                        active={activeIndexs.includes(3)}
                        index={3}
                        onClick={this.handleClick}
                        style={{fontSize: 24, fontFamily: "Arial"}}
                        >
                        <Icon name="dropdown" />
                        Kiểm soát sinh học
                    </Accordion.Title>
                    <Accordion.Content active={activeIndexs.includes(3)} >
                        <p style={{fontSize: 18, fontFamily: "Arial", textAlign:"justify"}}>
                            {data.biologicalControl}
                        </p>
                    </Accordion.Content>

                    <Accordion.Title
                        active={activeIndexs.includes(4)}
                        index={4}
                        onClick={this.handleClick}
                        style={{fontSize: 24, fontFamily: "Arial"}}
                        >
                        <Icon name="dropdown" />
                        Kiểm soát bằng hóa chất
                    </Accordion.Title>
                    <Accordion.Content active={activeIndexs.includes(4)} >
                        <p style={{fontSize: 18, fontFamily: "Arial", textAlign:"justify"}}>
                            {data.chemicalControl}
                        </p>
                    </Accordion.Content>
                    <Accordion.Title
                        active={activeIndexs.includes(5)}
                        index={5}
                        onClick={this.handleClick}
                        style={{fontSize: 24, fontFamily: "Arial"}}
                        >
                        <Icon name="dropdown" />
                        Nguồn tham khảo
                    </Accordion.Title>
                    <Accordion.Content active={activeIndexs.includes(5)} >
                        <p style={{fontSize: 18, fontFamily: "Arial"}}>
                            Plantix.net
                        </p>
                    </Accordion.Content>
                </Accordion>
                </div>
            </div>
        );
    }
}

export default DiseaseDetails;