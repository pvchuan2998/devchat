import React, { Component } from 'react';
import { Map, TileLayer, Marker, withLeaflet} from 'react-leaflet';
import { Popup as PopupLeaflet } from 'react-leaflet';
import 'leaflet/dist/leaflet.css';
import Leaflet from 'leaflet';
import firebase from '../../firebase';
import {Card, Image, Item, Button, Icon, Breadcrumb} from 'semantic-ui-react';
import {Popup as PopupSemantic} from 'semantic-ui-react';
import moment from 'moment';
import Carousel from 'react-elastic-carousel';
import { Link } from "react-router-dom";
import DatePicker from "react-datepicker";
import MeasureControlDefault from 'react-leaflet-measure';
import * as Scroll from 'react-scroll';


import "react-datepicker/dist/react-datepicker.css";

delete Leaflet.Icon.Default.prototype._getIconUrl;

Leaflet.Icon.Default.mergeOptions({
    iconRetinaUrl: require('leaflet/dist/images/marker-icon-2x.png'),
    iconUrl:'https://image.flaticon.com/icons/svg/2950/2950124.svg',
    shadowUrl: require('leaflet/dist/images/marker-shadow.png'),
    iconSize: new Leaflet.Point(40, 40)
});

//iconUrl: require('leaflet/dist/images/marker-icon.png'),
let scroll    = Scroll.animateScroll;

Leaflet.Icon.Default.imagePath = '//cdnjs.cloudflare.com/ajax/libs/leaflet/1.3.4/images/';

const timeoutLength = 10000

const measureOptions = {
    position: 'topright',
    primaryLengthUnit: 'meters',
    secondaryLengthUnit: 'kilometers',
    primaryAreaUnit: 'sqmeters',
    secondaryAreaUnit: 'acres',
    activeColor: '#db4a29',
    completedColor: '#9b2d14'
  };

  const MeasureControl = withLeaflet(MeasureControlDefault);

class MapComponent extends Component {

    _isMounted = false;

    constructor() {
        super();
        this.state = {
          lat: 9.465228,
          lng: 105.958107,
          zoom: 10,
          data: [],
          orchardData: {},
          isOpen: false,
          startDate: new Date(),
          endDate: new Date(),
          loading: false
        };

        this.startDateChanged = this.startDateChanged.bind(this);
        this.clearStartDate = this.clearStartDate.bind(this);
        this.endDateChanged = this.endDateChanged.bind(this);
        this.clearEndDate = this.clearEndDate.bind(this);
        //this.fetchData = this.fetchData.bind(this);
      }

    handleOpen = () => {
        this.setState({ isOpen: true })

        this.timeout = setTimeout(() => {
            this.setState({ isOpen: false })
        }, timeoutLength)
    }

    handleClose = () => {
        this.setState({ isOpen: false })
        clearTimeout(this.timeout)
    }

    startDateChanged(d){
        this.setState({startDate: d});
        console.log(this.formatDate(this.state.startDate))
        console.log(parseInt((new Date(this.state.startDate).getTime() / 1000).toFixed(0)))
    }
    
    clearStartDate(){
        this.setState({startDate: null});
        console.log(this.formatDate(this.state.endDate))
        
    }

    endDateChanged(d){
        this.setState({endDate: d});
        console.log(this.formatDate(this.state.endDate))
    }
    
    clearEndDate(){
        this.setState({endDate: null});
    }

    formatDate(date) {
        var d = new Date(date),
            month = '' + (d.getMonth() + 1),
            day = '' + d.getDate(),
            year = d.getFullYear();
    
        if (month.length < 2) 
            month = '0' + month;
        if (day.length < 2) 
            day = '0' + day;
    
        return [year, month, day].join('-');
    }

    searchGoogle(textToSearch) {
        var res = encodeURIComponent(textToSearch);
        window.open("https://www.google.com/#q=" + res);
        //let win = window.open("//" + "google.com/search?q=" + textToSearch, '_blank');
    }

    fetchOwnerData = (id) => {
        const db = firebase.firestore()

        db.collection("Orchard")
        .doc(id)
        .get()
        .then(doc => {
            const data = doc.data();
            
            if(this._isMounted){
                this.setState({ orchardData: data});
            }
            
            console.log(this.state.orchardData);
        
        });  
    
    }

    fetchData = (startDay, endDay) => {
        const db = firebase.firestore()
        var start = new Date(startDay)
        var end = new Date(endDay)
        this.setState({loading: true});
        db.collection("DiseasesDetectorResults")
        .where('time', '>', start)
        .where('time', '<', end)
        .get()
        .then(querySnapshot => {
            const data = querySnapshot.docs.map(doc => doc.data());
            if(this._isMounted){
                this.setState({data}); 
                this.setState({loading: false});
                console.log(this.state.data)
                console.log(this.state.data.map(data => data.lat)) 
                console.log(this.state.data.map(data => data.long)) 
            }
        });
       
        this.scrollToBottom();
      
    }


    componentDidMount() {
        this._isMounted = true;
        //this.fetchData();
    }
    

    componentWillUnmount() {
        this._isMounted = false;
    }

    renderPopup = (orchardData) => (
        <React.Fragment >
            <PopupSemantic.Header style={{fontFamily: "Arial"}}>Tên vườn: {orchardData && orchardData.name}</PopupSemantic.Header>
            <PopupSemantic.Content style={{fontFamily: "Arial"}}>
                <p>Địa chỉ vườn: {orchardData && orchardData.address}</p>
                <p>Diện tích: {orchardData && orchardData.area} (ha)</p>
                <p>Loại đất: {orchardData && orchardData.type}</p>
            </PopupSemantic.Content>
        </React.Fragment>  
    )

    scrollToBottom = () => {
        scroll.scrollToBottom();
      };

    render() {
        const {data, orchardData, startDate, endDate, loading} = this.state;
        const position = [this.state.lat, this.state.lng];
        return (
            <div>
                <div style={{textAlign:"center", marginTop: "15px"}}>
                    <span style={{fontFamily: "Arial", fontSize:"20px", marginRight:"10px"}}>Ngày bắt đầu:</span>

                    <DatePicker style={{fontSize:"20px"}} selected={this.state.startDate} onChange={this.startDateChanged} />

                    <input style={{marginLeft:"10px"}} className="ui inverted orange small button" type="button" onClick={this.clearStartDate} value="Xóa"/>
                </div>

                <div style={{marginTop: "15px", textAlign:"center"}}>
                    <span style={{fontFamily: "Arial", fontSize:"20px", marginRight:"10px"}}>Ngày kết thúc:</span>

                    <DatePicker  selected={this.state.endDate} onChange={this.endDateChanged} />

                    <input style={{marginLeft:"10px"}} className="ui inverted orange small button" type="button" onClick={this.clearEndDate} value="Xóa"/>
                </div>
                <div style={{textAlign:"center"}}>
                <Button style={{marginTop:"20px", marginBottom:"10px", marginBottom:120}} size="large" color="orange" 
                        disabled={loading}
                        className={loading ? "loading" : ""} 
                         onClick={() => {this.fetchData(this.formatDate(startDate), this.formatDate(endDate))}} 
                        >Xem dữ liệu</Button>
                </div>
                

                 <Map  style={{ height: '100vh', width: '70wh'}} center={position} zoom={this.state.zoom} ref="map" >
                    <TileLayer
                    url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
                    attribution="&copy; <a href=&quot;http://osm.org/copyright&quot;>OpenStreetMap</a> contributors"
                    />
                    <MeasureControl {...measureOptions} />
                    {data.map((data, key) =>  
                        <Marker position={[data.lat, data.long]} >
                            <PopupLeaflet maxWidth="700px" maxHeight="600px">
                                <Card style={{width:"450px",margin: "5px auto", fontFamily: "Arial"}} key={key}>
                                    <Card.Content>
                                        
                                        <Card.Header style={{fontSize:22}}>Tên vườn: {data.orchardId}</Card.Header>
                                       
                                        <PopupSemantic 
                                            trigger={
                                                <Button style={{color: "black", fontFamily:"Arial", fontSize:"12px"}} 
                                                 onClick={()=> this.fetchOwnerData(data.orchardId)} 
                                                 size="mini" inverted color='purple' icon labelPosition='right'> Xem thông tin vườn
                                                 <Icon name='purple right arrow' />
                                                </Button>
                                            }
                                            on='click'
                                            open={this.state.isOpen}
                                            onClose={this.handleClose}
                                            onOpen={this.handleOpen}
                                            position='top right'
                                            content={this.renderPopup(orchardData)}
                                        />
                                            
                                        
                                        <Card.Description style={{fontSize:"14px", marginTop:10}}>Loại cây trồng: <span style={{color:"red",fontFamily:"Arial", fontSize:"14px"}}>{data.plant}</span></Card.Description>
                                        <Card.Description style={{fontSize:"14px", marginTop:10}}>
                                            Loại sâu bệnh có thể đang gặp phải: <span title="Nhấn vào để tìm kiếm Google" 
                                                                                onClick={() => this.searchGoogle(data.diagnosedDisease)} 
                                                                                style={{color:"red", cursor:"pointer"}}>{data.diagnosedDisease}</span>
                                        </Card.Description >
                                        <Button size="mini" inverted color='purple' icon labelPosition='right'>
                                            <Link style={{color: "black", fontFamily:"Arial", fontSize:"12px"}} to={`/library/${data.diagnosedDisease.replace(/ /g,'')}`}> Xem chi tiết về bệnh </Link>
                                            <Icon name='purple right arrow' />
                                        </Button>
                                        <Card.Description style={{fontSize:"14px", marginTop:10}}>
                                            Thời gian: <span style={{color:"red", fontSize:"14px"}}> {moment( data.time && data.time.toDate()).format('MMMM Do YYYY, h:mm:ss a')}</span>
                                        </Card.Description>
                                        
                                        <Card.Description style={{fontSize:"14px", marginTop:10}}>
                                            Diện tích vùng bị nhiễm: <span style={{color:"red", fontSize:"14px"}}>{data.infectedArea} (m2)</span>
                                        </Card.Description>
                                        <Card.Description style={{fontSize:"14px", marginTop:10}}>
                                            Hình ảnh vùng bị nhiễm:
                                            <Carousel itemsToShow={1} style={{marginTop:10}}>
                                                <Item>
                                                    <Image
                                                        floated='right'
                                                        size='medium'
                                                        src={data.infectedAreaImgUri}
                                                    />
                                                </Item>
                                                <Item>
                                                    <Image
                                                        floated='right'
                                                        size='medium'
                                                        src={data.imgUri}
                                                    />
                                                </Item>
                                                
                                            </Carousel>
                                        </Card.Description> 
                                    </Card.Content>
                                </Card>
                            </PopupLeaflet>
                        </Marker>
                        
                    )}
                    
                </Map>
            </div>
           
        );
    }
}

export default (MapComponent);