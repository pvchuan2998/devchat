import React, { Component} from 'react';
import LineChart from './LineChart';
import firebase from '../../firebase';
import {Card, Breadcrumb} from "semantic-ui-react";
import OwnerCard from './OwnerCard';
import OrchardInfo from './OrchardInfo';
import { Link } from "react-router-dom";



class ChartPage extends Component {

    _isMounted = false;

    constructor(props) {
        super(props);

        this.state = {
            data: [],
            userId: firebase.auth().currentUser.uid,
            currentOrchardId: '7ZRaTN3bjdYRgqPKF30C',
            idList: [],
            currentOrchardData: {},
            currentOwnerId: ''
        }

        this.handleChange = this.handleChange.bind(this);
        this.handleChangePlant = this.handleChangePlant.bind(this);
    }

    fetchCurrentOrchard = (id) => {
        const db = firebase.firestore()

        db.collection("Orchard")
        .doc(id)
        .get()
        .then(doc => {
            const data = doc.data();
            
            if(this._isMounted){
                this.setState({ currentOrchardData: data});
                this.setState({currentOwnerId: data.owner});
            }
            
        });
    }

    componentDidMount() {
        this._isMounted = true;

        const db = firebase.firestore()

        db.collection("Orchard")
        //.where('staffID', '==', this.state.userId)
        .get()
        .then(querySnapshot => {
            const data = querySnapshot.docs.map(doc => doc.data());
            const id = querySnapshot.docs.map(doc => doc.id);
            if(this._isMounted){
                this.setState({ data: data, idList: id});
            }
        });

        this.fetchCurrentOrchard(this.state.currentOrchardId);

    }

    componentDidUpdate(prevProps, prevState) {
        if (prevState.currentOrchardId!== this.state.currentOrchardId) {
            this.fetchCurrentOrchard(this.state.currentOrchardId);
        }
    }

    componentWillUnmount() {
        this._isMounted = false;
      }

    handleChange(event) {
        
        this.setState({currentOrchardId: event.target.value});
      
    }
    handleChangePlant(event) {
        
        this.setState({currentPlant: event.target.value});
  
    }

    handleSubmit(event) {
        alert('Your choice is: ' + this.state.currentOrchardId);
        event.preventDefault();
    }
    
    render() {
        const {idList, currentOrchardId, currentOrchardData, currentOwnerId} = this.state;
        
        return (
            <div style={{width: "100%", background: "#ffffff", height:"100%"}}>
                {/* <Breadcrumb size="massive" style={{fontFamily:"Arial", margin:20}}>
                    <Breadcrumb.Section link><Link to="/">Trang chủ</Link></Breadcrumb.Section>
                    <Breadcrumb.Divider icon='right chevron' />
                    <Breadcrumb.Section active>Biểu đồ dữ liệu</Breadcrumb.Section>
                </Breadcrumb> */}
                <Card style={{width: "40%", margin: "10px auto"}}>
                    <Card.Content >
                        <form onSubmit={this.handleSubmit}>
                            <label style={{fontSize: "22px"}}>
                            Chọn mã khu vườn:
                            <select onChange={this.handleChange} value={this.state.currentOrchardId}>
                                {idList.map(id => 
                                    <option value={id} key={id} >
                                        {id}
                                    </option>
                                )}
                            </select>
                            </label>
                        </form>
                    </Card.Content>
                </Card>
                
                <OrchardInfo currentOrchardData={currentOrchardData}/>

                <OwnerCard currentOwnerId={currentOwnerId}/>
            
                <LineChart currentOrchard={currentOrchardId}/>

               
            </div> 
        );
    }
}

export default ChartPage;