const functions = require('firebase-functions');
const admin = require('firebase-admin');
admin.initializeApp(functions.config().firebase);
const cors = require('cors')({origin: true});

exports.getYearlyData = functions.https.onRequest((req, res) => {
    cors(req, res, () => {
        var stuff = [];
        var db = admin.firestore();
        
        var directories = req.path.split("/");
        var orchardId = directories[(directories.length - 3)];
        var start = directories[(directories.length - 2)];
        var end = directories[(directories.length - 1)];
        var startDay = new Date(start)
        var endDay = new Date(end)
        console.log(orchardId)
        console.log(start)
        console.log(end)

        db.collection("KetQuaDo")
            .where('maSoKhuVuon', '==', orchardId)
            .where('thoiGianGui', '>=', startDay)
            .where('thoiGianGui', '<=', endDay)
            .get()
            .then(snapshot => {
                snapshot.forEach(doc => {
                    var newElement = {
                        "orchardId": doc.data().maSoKhuVuon,
                        "time": doc.data().thoiGianGui,
                        "airTemp": doc.data().nhietDoKK,
                        "airHumid": doc.data().doAmKK,
                        "soilTemp": doc.data().nhietDoDat,
                        "soilHumid": doc.data().doAmDat,
                        "ec": doc.data().doECDat,
                        "ph": doc.data().doPHDat,
                        "fruit": doc.data().loaiTrai,
                        "width": doc.data().kichThuocTrai_Rong,
                        "height": doc.data().kichThuocTrai_Dai
                    }
                    stuff = stuff.concat(newElement);
                });
            res.send(stuff);
            return "";
        }).catch(reason => {
            res.send(reason)
        })

    });
});


exports.pushNotificationAirSensorTemperature = functions.database.ref('/VuonSongLu/airSensor/{id}')
    .onWrite((change, context) => {
       
        //  Get the current value of what was written to the Realtime Database.
        const valueObject = change.after.val(); 
        console.log('Push notification event triggered', valueObject);

        let orchardName
        var db = admin.database();
        db.ref('/VuonSongLu').once('value').then((dataSnapshot) =>{
            console.log(dataSnapshot.val().name)
            orchardName = dataSnapshot.val().name
            
            if(valueObject.temp > 35 ){
                const body = `Nhiệt độ hiện tại ở ${orchardName} đang cao! (${valueObject.temp}\u00B0C)`
                 // Create a notification
    
                const payload = {
                    notification: {
                        title: "Nhiệt độ cao!",
                        body: body,
                        sound: "default"
                    }
                };
                //Create an options object that contains the time to live for the notification and the priority
                const options = {
                    priority: "high",
                    timeToLive: 60 * 60 * 24
                };
                console.log("OK OK OK");
                return admin.messaging().sendToTopic("pushNotificationsAirTemperature", payload, options)    
            }
            else if(valueObject.temp < 20 ){
                const body = `Nhiệt độ hiện tại ở ${orchardName} đang thấp! (${valueObject.temp}\u00B0C)`
                 // Create a notification
                const payload = {
                    notification: {
                        title: "Nhiệt độ thấp!",
                        body: body,
                        sound: "default"
                    }
                };
                //Create an options object that contains the time to live for the notification and the priority
                const options = {
                    priority: "high",
                    timeToLive: 60 * 60 * 24
                };
                console.log("OK OK OK");
                return admin.messaging().sendToTopic("pushNotificationsAirTemperature", payload, options)    
            }
            return ""
        })
        .catch(reason => {
            console.log(reason)
        })
        
        return "";
});

exports.pushNotificationAirSensorHumidity = functions.database.ref('/VuonSongLu/airSensor/{id}')
    .onWrite((change, context) => {
        
        //  Get the current value of what was written to the Realtime Database.
        const valueObject = change.after.val(); 
        console.log('Push notification event triggered', valueObject);
        
    
        let orchardName
        var db = admin.database();
        db.ref('/VuonSongLu').once('value').then((dataSnapshot) =>{
            console.log(dataSnapshot.val().name)
            orchardName = dataSnapshot.val().name

            if(valueObject.humid > 85 ){
                const body = `Độ ẩm hiện tại ở ${orchardName} đang cao! (${valueObject.humid}%)`
                 // Create a notification
                const payload = {
                    notification: {
                        title: "Độ ẩm cao!",
                        body: body,
                        sound: "default"
                    }
                };
                //Create an options object that contains the time to live for the notification and the priority
                const options = {
                    priority: "high",
                    timeToLive: 60 * 60 * 24
                };
                console.log("OK OK OK");
                return admin.messaging().sendToTopic("pushNotificationsAirHumidity", payload, options)    
            }
            else if(valueObject.humid < 35 ){
                const body = `Độ ẩm hiện tại ở ${orchardName} đang thấp! (${valueObject.humid}%)`
                 // Create a notification
                const payload = {
                    notification: {
                        title: "Độ ẩm thấp!",
                        body: body,
                        sound: "default"
                    }
                };
                //Create an options object that contains the time to live for the notification and the priority
                const options = {
                    priority: "high",
                    timeToLive: 60 * 60 * 24
                };
                console.log("OK OK OK");
                return admin.messaging().sendToTopic("pushNotificationsAirHumidity", payload, options)    
            }
            return ""
        })
        .catch(reason => {
            console.log(reason)
        })
        
        return "";
});

exports.pushNotificationPH = functions.database.ref('/VuonSongLu/phSensor/{id}')
    .onWrite((change, context) => {
        
        //  Get the current value of what was written to the Realtime Database.
        const valueObject = change.after.val(); 
        console.log('Push notification event triggered', valueObject);
        
    
        let orchardName
        var db = admin.database();
        db.ref('/VuonSongLu').once('value').then((dataSnapshot) => {
            console.log(dataSnapshot.val().name)
            orchardName = dataSnapshot.val().name

            if(valueObject.ph > 7.5 ){
                const body = `Độ pH hiện tại ở ${orchardName} đang cao! (${valueObject.ph})`
                 // Create a notification
                const payload = {
                    notification: {
                        title: "Độ pH cao!",
                        body: body,
                        sound: "default"
                    }
                };
                //Create an options object that contains the time to live for the notification and the priority
                const options = {
                    priority: "high",
                    timeToLive: 60 * 60 * 24
                };
                console.log("OK OK OK");
                return admin.messaging().sendToTopic("pushNotificationsPH", payload, options)    
            }
            else if(valueObject.ph < 5 ){
                const body = `Độ pH hiện tại ở ${orchardName} đang thấp! (${valueObject.ph})`
                 // Create a notification
                const payload = {
                    notification: {
                        title: "Độ pH thấp!",
                        body: body,
                        sound: "default"
                    }
                };
                //Create an options object that contains the time to live for the notification and the priority
                const options = {
                    priority: "high",
                    timeToLive: 60 * 60 * 24
                };
                console.log("OK OK OK");
                return admin.messaging().sendToTopic("pushNotificationsPH", payload, options)    
            }
            return ""
        })
        .catch(reason => {
            console.log(reason)
        })
        
        return "";
});


exports.pushNotificationSoilSensorTemperature = functions.database.ref('/VuonSongLu/soilSensor/{id}')
    .onWrite((change, context) => {
       
        //  Get the current value of what was written to the Realtime Database.
        const valueObject = change.after.val(); 
        console.log('Push notification event triggered', valueObject);

        let orchardName
        var db = admin.database();
        db.ref('/VuonSongLu').once('value').then((dataSnapshot) =>{
            console.log(dataSnapshot.val().name)
            orchardName = dataSnapshot.val().name
            
            if(valueObject.temp > 30 ){
                const body = `Nhiệt độ đất hiện tại ở ${orchardName} đang cao! (${valueObject.temp}\u00B0C)`
                 // Create a notification
    
                const payload = {
                    notification: {
                        title: "Nhiệt độ đất cao!",
                        body: body,
                        sound: "default"
                    }
                };
                //Create an options object that contains the time to live for the notification and the priority
                const options = {
                    priority: "high",
                    timeToLive: 60 * 60 * 24
                };
                console.log("OK OK OK");
                return admin.messaging().sendToTopic("pushNotificationsSoilTemperature", payload, options)    
            }
            else if(valueObject.temp < 15 ){
                const body = `Nhiệt độ đất hiện tại ở ${orchardName} đang thấp! (${valueObject.temp}\u00B0C)`
                 // Create a notification
                const payload = {
                    notification: {
                        title: "Nhiệt độ đất thấp!",
                        body: body,
                        sound: "default"
                    }
                };
                //Create an options object that contains the time to live for the notification and the priority
                const options = {
                    priority: "high",
                    timeToLive: 60 * 60 * 24
                };
                console.log("OK OK OK");
                return admin.messaging().sendToTopic("pushNotificationsSoilTemperature", payload, options)    
            }
            return ""
        })
        .catch(reason => {
            console.log(reason)
        })
        
        return "";
});

exports.pushNotificationSoilSensorHumidity = functions.database.ref('/VuonSongLu/soilSensor/{id}')
    .onWrite((change, context) => {
        
        //  Get the current value of what was written to the Realtime Database.
        const valueObject = change.after.val(); 
        console.log('Push notification event triggered', valueObject);
        
    
        let orchardName
        var db = admin.database();
        db.ref('/VuonSongLu').once('value').then((dataSnapshot) =>{
            console.log(dataSnapshot.val().name)
            orchardName = dataSnapshot.val().name

            if(valueObject.humid > 80 ){
                const body = `Độ ẩm đất hiện tại ở ${orchardName} đang cao! (${valueObject.humid}%)`
                 // Create a notification
                const payload = {
                    notification: {
                        title: "Độ ẩm đất cao!",
                        body: body,
                        sound: "default"
                    }
                };
                //Create an options object that contains the time to live for the notification and the priority
                const options = {
                    priority: "high",
                    timeToLive: 60 * 60 * 24
                };
                console.log("OK OK OK");
                return admin.messaging().sendToTopic("pushNotificationsSoilHumidity", payload, options)    
            }
            else if(valueObject.humid < 35 ){
                const body = `Độ ẩm đất hiện tại ở ${orchardName} đang thấp! (${valueObject.humid}%)`
                 // Create a notification
                const payload = {
                    notification: {
                        title: "Độ ẩm đất thấp!",
                        body: body,
                        sound: "default"
                    }
                };
                //Create an options object that contains the time to live for the notification and the priority
                const options = {
                    priority: "high",
                    timeToLive: 60 * 60 * 24
                };
                console.log("OK OK OK");
                return admin.messaging().sendToTopic("pushNotificationsSoilHumidity", payload, options)    
            }
            return ""
        })
        .catch(reason => {
            console.log(reason)
        })
        
        return "";
});

